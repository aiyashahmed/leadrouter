<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Auth\Middleware\EnsureEmailIsVerified;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\LeadspipelineController;
use App\Http\Controllers\IntegrationController;
use App\Http\Controllers\EmailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('auth/google', 'Auth\LoginController@redirectToGoogle')->name('auth/google');
// Route::get('auth/google/callback', 'Auth\LoginController@handleGoogleCallback')->name('auth/google/callback');

// Route::get('auth/google', [LoginController::class, 'redirectToGoogle'])->name('auth/google');
// Route::get('auth/google/callback', [LoginController::class, 'handleGoogleCallback'])->name('auth/google/callback');
// login with google
Route::get('auth/google', [LoginController::class, 'redirectToGoogle'])->name('login.google');
Route::get('auth/google/callback', [LoginController::class, 'handleGoogleCallback'])->name('auth/google/callback');

// login with facebook
Route::get('auth/facebook', [LoginController::class, 'redirectToFacebook'])->name('login.facebook');
Route::get('auth/facebook/callback', [LoginController::class, 'handleFacebookCallback'])->name('auth/facebook/callback');

Route::get('login_', function () {
	return view('auth.login_');
})->name('login_');

Route::get('register_', function () {
	return view('auth.register_');
})->name('register_');

Route::get('getdemo', function () {
	return view('pages.getdemo');
})->name('getdemo');


Auth::routes(['verify' => true]);

Route::get('/', function () {
	return view('welcome');
})->name('welcome');

// Route::get('policy', function () {
// 	return view('pages.policy');
// })->name('policy');

Route::get('policy', function () {
	return view('layouts.policy');
})->name('policy');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth','verified');

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home')->middleware('auth','verified');

Route::group(['middleware' =>'verified'], function () {

	Route::get('company_active', [CompanyController::class, 'active'])->name('company_active');
	Route::get('company_create', [CompanyController::class, 'create'])->name('company_create');
	Route::post('company_store', [CompanyController::class, 'store'])->name('company_store');

	Route::get('lead_create', [LeadController::class, 'create'])->name('lead_create');
	Route::get('leads_view', [LeadController::class, 'view'])->name('leads_view');
	Route::get('lead_setup', [LeadController::class, 'lead_setup'])->name('lead_setup');
	Route::get('new_lead_step', [LeadController::class, 'new_lead_step'])->name('new_lead_step');
	Route::post('lead_store', [LeadController::class, 'store'])->name('lead_store');
	Route::get('lead_edit', [LeadController::class, 'edit'])->name('lead_edit');
	Route::post('lead_update', [LeadController::class, 'update'])->name('lead_update');


	Route::get('leadspipeline', [LeadspipelineController::class, 'index'])->name('leadspipeline');
	Route::post('leadspipeline-add', [LeadspipelineController::class, 'store'])->name('leadspipeline_add');
	Route::post('leadspipeline-delete', [LeadspipelineController::class, 'deletePipeline']);
	// pipeline step name change
    Route::post('leadspipeline/title-change', [LeadspipelineController::class, 'updateName']);

	// leads import and  export
	Route::post('lead/import', [LeadController::class, 'leadsImport'])->name('lead/import');
	// Route::post('lead/import', [LeadController::class, 'leadsImport']);
	Route::get('export', 'App\Http\Controllers\LeadController@export')->name('export');

	// contact view export
	Route::get('contact_export', 'App\Http\Controllers\LeadController@contact_export')->name('contact_export');

//	lead step change
    Route::post('lead/step/change', [LeadController::class, 'stepChange']);




	// Route::get('lead_create', function () {
	// 	return view('lead.lead_create');
	// })->name('lead_create');

	Route::get('integrate', [IntegrationController::class, 'index'])->name('integrate');
	Route::get('integrate_voip/{name}', [IntegrationController::class, 'integrate_voip'])->name('integrate_voip');
	Route::get('integrade_voip_install', [IntegrationController::class, 'integrade_voip_install'])->name('integrade_voip_install');
	Route::post('ivoip_save', [IntegrationController::class, 'ivoip_save'])->name('ivoip_save');



	Route::get('contact_view', function () {
		return view('pages.contact_view');
	})->name('contact_view');

	Route::get('accounts_view', [EmailController::class, 'showEmails'])->name('accounts_view');

	Route::get('calendar_view', function () {
		return view('pages.calendar_view');
	})->name('calendar_view');

	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');


	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');

//	User email inbox

//    hot mail
    Route::post('email/hotmail', [EmailController::class, 'connectHotMail']);

    Route::get('email/get/inbox', [EmailController::class, 'getInbox']);
    Route::get('email/get/sendbox', [EmailController::class, 'getSendbox']);
    Route::get('email/get/trash', [EmailController::class, 'getTrash']);

//    gmail
    Route::get('email/gmail/connect', [EmailController::class, 'connectGmail']);
    Route::get('email/gmail/callback', [EmailController::class, 'handleGmailCallback']);
    Route::get('email/gmail/extract', [EmailController::class, 'extractGmailInbox']);

//    yahoo
    Route::get('email/yahoo/connect', [EmailController::class, 'connectYahoo']);
    Route::get('email/yahoo/callback', [EmailController::class, 'handleYahooCallback']);

//    compose mail
    Route::get('email/compose', [EmailController::class, 'compose']);
//    send mail
    Route::post('email/send/mail', [EmailController::class, 'sendMail']);

//    remove email accounts
    Route::post('email/remove/accounts', [EmailController::class, 'removeAccounts']);

});

Route::group(['middleware' => 'auth','verified'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware(['auth'])->name('verification.notice');


Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');



Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('status', 'verification-link-sent');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

