@include('layouts.navbars.navs.guest')
<style>
  .login-page {
    background-color: #fff !important;
    background-image: url('{{ asset('material') }}/img/bg.jpeg');
    height: 90%;
    background-repeat: no-repeat;
    align-items: center;
    background-position: top center;
  }
</style>
<div class="wrapper wrapper-full-page">
<div class="login-page header-filter"></div>

  <!-- <div class="login-page header-filter" style="background-color: #fff !important; background-image: url('{{ asset('material') }}/img/banner.jpeg'); height:90%;background-repeat: no-repeat;align-items: center;background-position: top center;"> -->
  <!-- <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('{{ asset('material') }}/img/login.jpg'); background-size: cover; background-position: top center;align-items: center;" data-color="purple"> -->
  <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
    @yield('content')
    @include('layouts.footers.guest')
  <!-- </div> -->
</div>
