<div class="wrapper ">
  @include('layouts.navbars.sidebar')
  @if ($activePage != 'company')
  @endif
  <div class="main-panel">
    @include('layouts.navbars.navs.auth')
    @if ($activePage != 'company')
    @endif
    @yield('content')
    @include('layouts.footers.auth')
  </div>
</div>