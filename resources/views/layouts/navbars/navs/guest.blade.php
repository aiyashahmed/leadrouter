<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent fixed-top text-white" style="border:none">
  <div class="container">
    <div class="navbar-wrapper">
      <a class="navbar-brand" href="{{ route('welcome') }}">
      <img height="200px" alt="100x100" src="{{ asset('material') }}/img/logo.png" style="margin-top:-90px; margin-left:-90px">
      </a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
      <ul class="navbar-nav">
        <li class="nav-item{{ $activePage == 'login' ? ' active' : '' }}">
          <a href="{{ route('login_') }}" class="nav-link">
            <i class="material-icons">fingerprint</i> {{ __('LOGIN') }}
          </a>
        </li>
        <li class="nav-item{{ $activePage == 'register' ? ' active' : '' }}">
          <a href="{{ route('getdemo') }}" class="nav-link">
            <i class="material-icons">book_online</i> {{ __('GET A DEMO') }}
          </a>
        </li>
        <li class="nav-item{{ $activePage == 'register' ? ' active' : '' }}">
          <a href="{{ route('register_') }}" class="nav-link">
            <i class="material-icons">touch_app</i> {{ __('TRY IT FREE') }}
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- End Navbar -->
