<!-- arshad added js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- arshad js end -->
<script>
  function profile_menu() {
    // Hide it but only if not hidden - hide
    $('#profileDiv:visible').animate({
      width: 'hide'
    }, 300);

    // Later in the script - Show it but only If it's not visible.
    $('#profileDiv:hidden').animate({
      width: 'show'
    }, 300);

  }

  $(document).mouseup(function(e) {
    var profileDiv = $("#profileDiv");
    // if the target of the click isn't the container nor a descendant of the container
    if (!profileDiv.is(e.target) && profileDiv.has(e.target).length === 0) {
      profileDiv.animate({
        width: 'hide'
      }, 300);
    }
  });
</script>
<style>
  #profileDiv {
    display: none;
  }

  .navbar {
    background-color: #fff;
    position: fixed;
    top: 0;
    left: 70px;
    right: 0;
    border-bottom: 1px solid #e8eaeb;
    box-sizing: border-box;
    height: 66px !important;
    z-index: 1;
  }

  .navbar-profileDiv {
    background-color: #0a1b2b;
    color: #fff;
    /* padding: 5px 2px 5px 2px; */
    width: 400px;
    position: fixed;
    z-index: 9999;
    top: 0;
    left: 70px;
    right: 0;
    height: 66px !important;
  }

  .profile1 {
    float: left;
    width: 60%;
    padding: 9px 2px 6px 10px;
    color: #F9E79F;
    font-size: 18px;
    font-family: inherit;
    background-color: #212F3D;
  }

  .profile2 {
    float: left;
    width: 20%;
    font-size: 14px;
    padding: 10px 2px 8px 10px;
    text-align: center;
    color: #a9afbb;
    text-decoration: none;
    background-color: #273746;
  }

  .profile3 {
    float: right;
    width: 20%;
    font-size: 14px;
    padding: 10px 2px 8px 10px;
    text-align: center;
    color: #a9afbb;
    text-decoration: none;
    background-color: #566573;
  }

  .profile1:hover {
    color: #fff;
    text-decoration: none;
  }

  .profile2:hover {
    color: #fff;
    text-decoration: none;
  }

  .profile3:hover {
    color: #fff;
    text-decoration: none;
  }

  .logo {
    height: 66px !important;
  }
</style>

<div class="sidebar" data-color="trans" data-background-color="white">
  <!-- <div class="sidebar" data-color="green" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-3.jpg"> -->

  <!-- <div class="sidebar" data-color="azure" data-background-color="white"> -->
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <!-- <span style="font-weight: lighter; color:#03af0b">CRM</span> -->
    <div id="navbarDropdownProfile" onclick="profile_menu()">
      <center>
        @if(is_null(Auth::user()->avatar))
        <img height="50px" alt="profile" src="{{ asset('material') }}/img/faces/avatar.png" data-holder-rendered="true">
        @else
        <img height="50px" alt="profile" src="@php echo Auth::user()->avatar; @endphp" data-holder-rendered="true">
        @endif
      </center>
    </div>
    <div class="navbar-profileDiv" id="profileDiv">
      <a class="profile1" href="{{ route('profile.edit') }}">
        <b>
          @php
          echo Auth::user()->name;
          @endphp
        </b></br>
        <span style="font-size: 14px; color:#fff">
          @php
          echo "USER ID ".Auth::user()->id;
          @endphp
        </span>
      </a>
      <a class="profile2" href="{{ route('profile.edit') }}">
        <i class="material-icons">manage_accounts</i>
        </br>
        {{ __('profile') }}
      </a>
      <a class="profile3" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
        <i class="material-icons">logout</i>
        </br>
        {{ __('logout') }}
      </a>
    </div>
    <!-- <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownProfile" style="margin-top: -20px;">
          <div class="dropdown-header"><span style="text-align:left">
              @php
              echo Auth::user()->name;
              @endphp
            </span>
            <span style="text-align:right">
              &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;ID: @php
              echo Auth::user()->id;
              @endphp
            </span>
          </div>

          <a class="dropdown-item" href="{{ route('profile.edit') }}">{{ __('Profile') }}</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Log out') }}</a>
        </div> -->
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav" id="menu">
      <li class="nav-item">
        <div class="row">
          <div class="col-3">
            <a class="nav-link" href="{{ route('home') }}">
              <div class="row">
                <i class="material-icons {{ $activePage == 'dashboard' ? 'text-warning' : '' }}">dashboard</i>
              </div>
              <div class="row mt-2">
                <p>{{ __('Dashboard') }}</p>
              </div>
            </a>
          </div>
        </div>
      </li>
      <li class="nav-item hoverable-li">
        <div class="row">
          <div class="col-3">
            <a class="nav-link" href="{{ route('leads_view') }}">
              <div class="row">
                <i class="material-icons {{ ($activePage == 'leads_view') ? 'text-warning' : '' }}">addchart</i>
              </div>
              <div class="row mt-2">
                <p>{{ __('Leads') }}</p>
              </div>
            </a>
          </div>
          <div class="col">
            <ul class="text-center hoverable-ul">
              <a href="#">Leads</a>
              <hr>
              <li><a href="grids.html">Pipeline</a></li>
              <li><a href="portlet.html">Setup</a></li>
            </ul>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <div class="row">
          <div class="col-3">
            <a class="nav-link" href="{{ route('contact_view') }}">
              <div class="row">
                <i class="material-icons {{ $activePage == 'contact_view' ? 'text-warning' : '' }}">contacts</i>
              </div>
              <div class="row mt-2">
                <p>{{ __('Contact') }}</p>
              </div>
            </a>
          </div>
          <div class="col">
          </div>
        </div>
      </li>
      <!-- <li class="nav-item{{ $activePage == 'accounts_view' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('accounts_view') }}">
          <i class="material-icons">request_page</i>
          <p>{{ __('Accounts') }}</p>
        </a>
      </li> -->
      <li class="nav-item">
        <div class="row">
          <div class="col-3">
            <a class="nav-link" href="{{ route('calendar_view') }}">
              <div class="row">
                <i class="material-icons {{ $activePage == 'calendar_view' ? 'text-warning' : '' }}">assignment_ind</i>
              </div>
              <div class="row mt-2">
                <p>{{ __('Calendar') }}</p>
              </div>
            </a>
          </div>
          <div class="col">
          </div>
        </div>
      </li>
      <li class="nav-item hoverable-li">
        <div class="row">
          <div class="col-3">
            <a class="nav-link" href="{{ route('accounts_view') }}">
              <div class="row">
                <i class="material-icons {{ $activePage == 'accounts_view' ? 'text-warning' : '' }}">mail</i>
              </div>
              <div class="row mt-2">
                <p>{{ __('Mail') }}</p>
              </div>
            </a>
          </div>
          <div class="col">
            <ul class="text-center hoverable-ul">
              <a href="#">Email</a>
              <hr>
              <li><a href="#">Inbox</a></li>
              <li><a href="#">Sent</a></li>
              <li><a href="#">Trash</a></li>
            </ul>
          </div>
        </div>
      </li>
      <!-- <li class="nav-item{{ $activePage == 'integrate' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('integrate') }}">
          <i class="material-icons">close_fullscreen</i>
          <p><b>{{ __('Integrate') }}</b></p>
        </a>
      </li> -->
      <hr style="color:deepskyblue; margin:-1px 0px">
      <li class="nav-item" style="position: absolute; bottom: 0; left:-17px; right:0;">
        <a class="nav-link" href="">
          <i class="material-icons">notifications_active</i>
        </a>
      </li>
      <!-- <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('table') }}">
          <i class="material-icons">content_paste</i>
          <p>{{ __('Table List') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'typography' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('typography') }}">
          <i class="material-icons">library_books</i>
          <p>{{ __('Typography') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'icons' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('icons') }}">
          <i class="material-icons">bubble_chart</i>
          <p>{{ __('Icons') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'map' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('map') }}">
          <i class="material-icons">location_ons</i>
          <p>{{ __('Maps') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'notifications' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('notifications') }}">
          <i class="material-icons">notifications</i>
          <p>{{ __('Notifications') }}</p>
        </a>
      </li> -->

    </ul>
  </div>
</div>
<nav class="navbar navbar-expand-lg">
  <div class="container">

    <!-- <div>
      <a class="navbar-brand" href="{{ route('home') }}">
        <img height="150px" alt="LEADSROUTER" src="{{ asset('material') }}/img/logo.png" style="margin-top:-60px; margin-left:-100px">
      </a>
    </div> -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">

    </div>
  </div>
</nav>