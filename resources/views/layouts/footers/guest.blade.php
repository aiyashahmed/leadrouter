<footer class="footer" style="border:none">
    <div class="container">
        <nav class="float-center">
            <ul>
                <li>
                    <a href="{{ route('register') }}" class="btn btn-primary btn-circle btn-sm"
                       style="font-size: 18px; margin:0px 25px; border-radius: 35px;">
                        {{ __('TRY FOR FREE') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('register') }}" class="btn btn-dark btn-circle btn-sm"
                       style="font-size: 18px; margin:0px 25px; border-radius: 35px;">
                        {{ __('LEARN MORE') }}
                        <i class="material-icons" style="font-size: 25px;">double_arrow</i>
                    </a>
                </li>

            </ul>

        </nav>
        <div class="text-right">
            <a href="{{ route('policy') }}" style="font-size: 16px; text-decoration:underline; color:blue">
                {{ __('terms-conditions') }}
            </a>
        </div>
    </div>
</footer>
