@extends('layouts.app', ['activePage' => 'leads_view', 'titlePage' => __('Leads View')])

@section('content')
    <style>
        .navbar {
            background-color: #fff;
            position: fixed;
            top: 0;
            left: 91px;
            right: 0;
            border-bottom: 1px solid #e8eaeb;
            box-sizing: border-box;
            height: 66px !important;
            z-index: 9999;
        }

        .counter.counter-lg {
            top: -24px !important;
        }

        .sidenav {
            height: 100%;
            width: 300px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #F8F9F9;
            overflow-x: hidden;
            padding-top: 20px;
            margin-left: 91px;
            margin-top: 61px;
            border-right: 1px solid #fff;
            opacity: 0.2;
        }

        .middlediv {
            height: 100%;
            width: 250px;
            position: fixed;
            z-index: 2;
            top: 0;
            left: 0;
            background-color: #F4F6F7;
            overflow-x: hidden;
            padding-top: 20px;
            margin-left: 390px;
            margin-top: 61px;
            box-shadow: -2px 5px 10px 0 #888;
        }

        .main {
            margin-left: 550px;
        }
    </style>
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="page-heading">
                    <a href="#">LEADS SETUP</a>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('integrate') }}">
                            <button type="button" class="btn btn-md"
                                    style="background-color:#fff; color:#ff9800; letter-spacing:1px">
                                <b>X</b> close
                            </button>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="content" style="margin-top: 45px;">
        <div class="container-fluid">
            <div class="row">
                <div class="sidenav" style="padding:5px; font-weight:bolder">
                    <a class="nav-link active" href="{{ route('integrate') }}">
                        <button type="button" class="btn btn-md btn-warning">
                            + Add Source
                        </button>
                    </a>
                </div>
                <div class="middlediv shadow-lg p-3" style="padding:5px; font-weight:bolder">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-stats" style="background-color:#fff">
                                <div class="card-icon"
                                     style="text-align:center; font-size:29px; margin:20px 10px; padding:5px;">
                                    <img height="90px" width="95%" alt="voip"
                                         src="{{ asset('material') }}/img/logo/asterisk.png"
                                         data-holder-rendered="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="page-heading" style="font-weight: bolder; font-size:29px;">
                                <i class="fa fa-asterisk" style="color:#ff9800; text-align:center; font-size:29px;"></i>
                                <a href="#"><b>Asterisk</b></a>
                            </div>
                            <p style="font-weight: lighter;">Terms of service are the legal agreements between a service
                                provider and a person who wants to use that service. The person must agree to abide by
                                the terms of service in order to use the offered service.</p>

                        </div>
                        <a class="nav-link" href="{{route('integrade_voip_install')}}">
                            <button type="button" class="btn btn-md btn-warning">
                                + INSTALL
                            </button>
                        </a>
                        <hr>
                    </div>

                </div>
                <div class="main">
                    <div class="row">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-11">
                            <p style="font-weight: lighter;">Terms of service are the legal agreements between a service
                                provider and a person who wants to use that service. The person must agree to abide by
                                the terms of service in order to use the offered service.</p>
                            <p style="font-weight: lighter;">Terms of service are the legal agreements between a service
                                provider and a person who wants to use that service. The person must agree to abide by
                                the terms of service in order to use the offered service.</p>
                            <br>
                            <p style="font-weight: lighter;">Terms of service are the legal agreements between a service
                                provider and a person who wants to use that service. The person must agree to abide by
                                the terms of service in order to use the offered service.</p>
                            <p style="font-weight: lighter;">Terms of service are the legal agreements between a service
                                provider and a person who wants to use that service. The person must agree to abide by
                                the terms of service in order to use the offered service.</p>
                            <p style="font-weight: lighter;">Terms of service are the legal agreements between a service
                                provider and a person who wants to use that service. The person must agree to abide by
                                the terms of service in order to use the offered service.</p>
                            <br>
                            <p style="font-weight: lighter;">Terms of service are the legal agreements between a service
                                provider and a person who wants to use that service. The person must agree to abide by
                                the terms of service in order to use the offered service.</p>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
