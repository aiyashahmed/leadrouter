@extends('layouts.app', ['activePage' => 'leads_view', 'titlePage' => __('Leads View')])

@section('content')
    <style>
        .navbar {
            background-color: #fff;
            position: fixed;
            top: 0;
            left: 70px;
            right: 0;
            border-bottom: 1px solid #e8eaeb;
            box-sizing: border-box;
            height: 66px !important;
            z-index: 9999;
        }

        .counter.counter-lg {
            top: -24px !important;
        }

        .sidenav {
            height: 100%;
            width: 300px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #F8F9F9;
            overflow-x: hidden;
            padding-top: 20px;
            margin-left: 70px;
            margin-top: 61px;
        }

        .main {
            margin-left: 300px;
        }

        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }

        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }

        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
    </style>
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="page-heading">
                    <a href="#">LEADS SOURCE</a>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('leads_view') }}">
                            <button type="button" class="btn btn-md"
                                    style="background-color:#fff; color:#ff9800; letter-spacing:1px">
                                BACK
                            </button>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="content" style="margin-top: 45px;">
        <div class="container-fluid">
            <div class="row">
                <div class="sidenav" style="padding:5px; font-weight:bolder">
                    <br>
                    INCOMING LEADS
                    <div class="material-switch pull-right">
                        <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox"/>
                        <label for="someSwitchOptionWarning" class="label-warning"></label>
                    </div>
                </div>
                <div class="main">
                    VOICE SERVICES
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats" style="background-color:#fff">
                                <div class="card-icon"
                                     style="text-align:center; font-size:31px; margin:10px; padding:5px;">
                                    <i class="fa fa-asterisk" style="color:#ff9800;"></i> Asterisk
                                </div>
                                <div class="card-footer">
                                    <a aria-disabled="true" class="btn btn-round btn-fill btn-info"
                                       href="{{ route('integrate_voip', ['name' => 'asterisk']) }}"
                                       style="margin-left: 20px;">+ADD SOURCE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats" style="background-color:#fff">
                                <div class="card-icon"
                                     style="text-align:center; font-size:29px; margin:10px; padding:5px;">
                                    <img style="width:175px" alt="LEADSROUTER"
                                         src="{{ asset('material') }}/img/logo/ringcentral.png">
                                </div>
                                <div class="card-footer">
                                    <a aria-disabled="true" class="btn btn-round btn-fill btn-info"
                                       href="{{ route('integrate_voip', ['name' => 'ring central']) }}"
                                       style="margin-left: 20px;">+ADD SOURCE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats">
                                <div class="card-icon"
                                     style="text-align:center; font-size:29px; margin:10px; padding:5px;">
                                    <img class="img-responsive" height="30px" style="width:120px" alt="100x100"
                                         src="{{ asset('material') }}/img/logo/twilio.png" data-holder-rendered="true">
                                </div>
                                <div class="card-footer">
                                    <a aria-disabled="true" class="btn btn-round btn-fill btn-info"
                                       href="{{ route('integrate_voip', ['name' => 'twilio']) }}"
                                       style="margin-left: 20px;">+ADD SOURCE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="background-color: #000; color:#b2b2b2">
                    MAIL AND SMS
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats" style="background-color:#F1C40F;">
                                <div class="card-icon"
                                     style="text-align:center; font-size:29px; margin:10px; padding:5px;">
                                    <img class="img-responsive" height="40px" alt="100x100"
                                         src="{{ asset('material') }}/img/logo/mailchimp.png"
                                         data-holder-rendered="true">
                                </div>
                                <div class="card-footer">
                                    <a aria-disabled="true" class="btn btn-round btn-fill btn-info"
                                       href="{{ route('integrate_voip', ['name' => 'server monkey']) }}"
                                       style="margin-left: 20px;">+ADD SOURCE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats" style="background-color:#d8d8d8;">
                                <label class="badge text-danger mt-2" id="email-connect-error"></label>
                                <div class="card-icon"
                                     style="text-align:center; font-size:29px; margin:10px; padding:5px;">
                                    <img class="img-responsive" height="40px" alt="100x100"
                                         src="{{ asset('material') }}/img/logo/email.png" data-holder-rendered="true">
                                </div>
                                <div class="card-footer">
                                    <a href="{{ route('integrate_voip', ['name' => 'email']) }}"
                                       class="btn btn-round btn-fill btn-info" style="margin-left: 20px;">+ADD
                                        SOURCE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="background-color: #000; color:#b2b2b2">
                    ONLINE CHATS
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats bg-info">
                                <div class="card-icon"
                                     style="text-align:center; font-size:29px; margin:10px; padding:5px;">
                                    <i class="fa fa-facebook-f"></i> FACEBOOK
                                </div>
                                <div class="card-footer">
                                    <a aria-disabled="true" class="btn btn-round btn-fill btn-info"
                                       href="{{ route('integrate_voip', ['name' => 'facebook']) }}"
                                       style="margin-left: 20px;">+ADD SOURCE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats" style="background-color:#1ABC9C; color:#fff">
                                <div class="card-icon"
                                     style="text-align:center; font-size:21px; margin:10px; padding:7px;">
                                    <i class="fa fa-whatsapp"></i> WHATSAPP
                                </div>
                                <div class="card-footer">
                                    <a class="btn btn-round btn-fill btn-info"
                                       href="{{ route('integrate_voip', ['name' => 'whatsapp']) }}"
                                       style="margin-left: 20px;">+ADD SOURCE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats" style="background-color:#8E44AD;  color:#fff">
                                <div class="card-icon"
                                     style="text-align:center; font-size:29px; margin:10px; padding:7px;">
                                    <i class="fab fa-viber"></i> VIBER
                                </div>
                                <div class="card-footer">
                                    <a aria-disabled="true" class="btn btn-round btn-fill btn-info"
                                       href="{{ route('integrate_voip', ['name' => 'viber']) }}"
                                       style="margin-left: 20px;">+ADD SOURCE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
