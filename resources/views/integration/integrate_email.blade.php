@extends('layouts.app', ['activePage' => 'leads_view', 'titlePage' => __('Leads View')])

@section('content')
    <style>
        .navbar {
            background-color: #fff;
            position: fixed;
            top: 0;
            left: 91px;
            right: 0;
            border-bottom: 1px solid #e8eaeb;
            box-sizing: border-box;
            height: 66px !important;
            z-index: 9999;
        }

        .counter.counter-lg {
            top: -24px !important;
        }

        .sidenav {
            height: 100%;
            width: 300px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #F8F9F9;
            overflow-x: hidden;
            padding-top: 20px;
            margin-left: 91px;
            margin-top: 61px;
            border-right: 1px solid #fff;
        }

        .middlediv {
            height: 100%;
            width: 300px;
            position: fixed;
            z-index: 2;
            top: 0;
            left: 0;
            background-color: #F4F6F7;
            overflow-x: hidden;
            padding-top: 20px;
            margin-left: 390px;
            margin-top: 61px;
            box-shadow: -2px 5px 10px 0 #888;
        }

        .card-icon {
            text-align: center;
            font-size: 29px;
            /*margin: 20px 10px;*/
            padding: 5px;
            width: 100%;
        }

        .main {
            margin-left: 550px;
        }

        .progress-bar {
            background-color: #fab95a;
        }
    </style>
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="page-heading">
                    <a href="#">LEADS SETUP</a>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('integrate') }}">
                            <button type="button" class="btn btn-md"
                                    style="background-color:#fff; color:#ff9800; letter-spacing:1px">
                                <b>X</b> close
                            </button>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="content" style="margin-top: 45px;">
        <div class="container-fluid">
            <div class="row">
                <div class="sidenav" style="padding:5px; font-weight:bolder">
                    <div class="nav-link">
                        <button id="gmail-btn" type="button" class="btn btn-md btn-outline-warning">
                            + Gmail
                        </button>
                    </div>
                    <div class="nav-link">
                        <button id="yahoo-btn" type="button" class="btn btn-md btn-outline-warning">
                            + Yahoo
                        </button>
                    </div>
                    <div class="nav-link active">
                        <button id="imap-btn" type="button" class="btn btn-md btn-outline-warning">
                            + IMAP / SMTP
                        </button>
                    </div>
                </div>
                <div id="gmail-div" class="setup-div">
                    <div class="middlediv shadow-lg p-3" style="padding:5px; font-weight:bolder">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card card-stats" style="background-color:#fff">
                                    <div class="card-icon">
                                        <img height="90px" alt="gmail"
                                             src="{{ asset('material') }}/img/logo/gmail.png"
                                             data-holder-rendered="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="page-heading" style="font-weight: bolder; font-size:29px;">
                                    <a href="#"><b>Gmail</b></a>
                                </div>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <hr>
                                <a href="{{url('email/gmail/connect')}}"
                                   class="btn btn-sm btn-warning">+ Install</a>
                            </div>
                        </div>

                    </div>
                    <div class="main">
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-11">
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <br>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <br>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div id="yahoo-div" class="d-none setup-div">
                    <div class="middlediv shadow-lg p-3" style="padding:5px; font-weight:bolder">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card card-stats" style="background-color:#fff">
                                    <div class="card-icon">
                                        <img height="90px" alt="yahoo mail"
                                             src="{{ asset('material') }}/img/logo/yahoo.png"
                                             data-holder-rendered="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="page-heading" style="font-weight: bolder; font-size:29px;">
                                    <a href="#"><b>Yahoo</b></a>
                                </div>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>

                            </div>
                            <a class="nav-link" href="{{route('integrade_voip_install')}}">
                                <button type="button" class="btn btn-md btn-warning">
                                    + INSTALL
                                </button>
                            </a>
                            <hr>
                        </div>

                    </div>
                    <div class="main">
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-11">
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <br>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <br>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div id="imap-div" class="d-none setup-div">
                    <div class="middlediv shadow-lg p-3" style="padding:5px; font-weight:bolder">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card card-stats" style="background-color:#fff">
                                    <div class="card-icon">
                                        <img height="90px" alt="smtp"
                                             src="{{ asset('material') }}/img/logo/email.png"
                                             data-holder-rendered="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="page-heading" style="font-weight: bolder; font-size:29px;">
                                    <a href="#"><b>IMAP / SMTP</b></a>
                                </div>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>

                            </div>
                            <div class="nav-link">
                                <button type="button" class="btn btn-md btn-warning" data-toggle="modal"
                                        data-target="#email-setting-modal">
                                    + INSTALL
                                </button>
                            </div>
                            <hr>
                        </div>

                    </div>
                    <div class="main">
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-11">
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <br>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>
                                <br>
                                <p style="font-weight: lighter;">Terms of service are the legal agreements between a
                                    service
                                    provider and a person who wants to use that service. The person must agree to abide
                                    by
                                    the terms of service in order to use the offered service.</p>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{--        imap login credential modal --}}
    <div class="modal fade" id="email-setting-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
         aria-hidden="true" style="margin-top: 100px">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="card">
                    <div class="card-header">
                        SMTP/IMAP Setting
                    </div>
                    <div class="card-body">
                        <div>
                            <label id="email-setting-error" class="badge text-danger"></label>
                        </div>
                        <form id="email-setting-form">
                            <div class="form-group">
                                <input type="text" class="form-control" name="host" placeholder="Host">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-outline-warning btn-sm" id="connect-btn">Connect
                            </button>

                            <div class="progress mt-2 d-none">
                                <div class="progress-bar">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
