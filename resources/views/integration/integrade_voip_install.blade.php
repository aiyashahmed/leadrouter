@extends('layouts.app', ['activePage' => 'leads_view', 'titlePage' => __('Leads View')])

@section('content')
<style>
    .counter.counter-lg {
        top: -24px !important;
    }

    .sidenav {
        height: 100%;
        width: 300px;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #F8F9F9;
        overflow-x: hidden;
        padding-top: 20px;
        margin-left: 91px;
        margin-top: 61px;
        border-right: 1px solid #fff;
        opacity: 0.2;
    }

    .middlediv {
        height: 100%;
        width: 250px;
        position: fixed;
        z-index: 2;
        top: 0;
        left: 0;
        background-color: #F4F6F7;
        overflow-x: hidden;
        padding-top: 20px;
        margin-left: 390px;
        margin-top: 61px;
        box-shadow: -2px 5px 10px 0 #888;
    }

    .main {
        margin-left: 550px;
    }
</style>
<nav class="navbar navbar-expand-lg">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="page-heading">
                <a href="#">LEADS SETUP</a>
            </div>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('integrate') }}">
                        <button type="button" class="btn btn-md" style="background-color:#fff; color:#ff9800; ; letter-spacing:1px">
                        <b>X</b> close
                        </button>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="content" style="margin-top: 45px;">
    <div class="container-fluid">
        <div class="row">
            <div class="sidenav" style="padding:5px; font-weight:bolder">
                <p>LEAD SOURCE LIST</p>
                <a class="nav-link" href="{{ route('integrate') }}">
                    <button type="button" class="btn btn-md btn-warning">
                        + Add Source
                    </button>
                </a>
            </div>
            <div class="middlediv shadow-lg p-3" style="padding:5px; font-weight:bolder">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-stats" style="background-color:#fff">
                            <div class="card-icon" style="text-align:center; font-size:29px; margin:20px 10px; padding:5px;">
                                <img height="90px" width="95%" alt="voip" src="{{ asset('material') }}/img/logo/asterisk.png" data-holder-rendered="true">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="page-heading" style="font-weight: bolder; font-size:29px;">
                            <i class="fa fa-asterisk" style="color:#ff9800; text-align:center; font-size:29px;"></i>
                            <a href="#"><b>Asterisk</b></a>
                        </div>
                        <p style="font-weight: lighter;">Terms of service are the legal agreements between a service provider and a person who wants to use that service. The person must agree to abide by the terms of service in order to use the offered service.</p>

                    </div>
                    <a class="nav-link" href="{{route('integrate')}}" disable>
                        <button type="button" class="btn btn-sm btn-warning">
                            UNINSTALL
                        </button>
                    </a>
                    <hr>
                </div>

            </div>
            <div class="main">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-11">
                        <div class="card">
                            <div class="card-header" style="background-color:#ff9800">
                                <h4 class="card-title text-white" style="font-size: 10px;">VOIP-SOFTPHONE INTEGRATION SETUP</h4>
                            </div>
                            <div class="card-body">
                                <form method="post" action="{{ route('ivoip_save') }}" autocomplete="off" class="form-horizontal" autocomplete="off">
                                    @csrf
                                    <!-- @method('put') -->
                                    @if (session('status'))
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="alert alert-success">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <i class="material-icons">close</i>
                                                </button>
                                                <span>{{ session('status') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group{{ $errors->has('user_name') ? ' has-danger' : '' }}">
                                                <input class="form-control" name="user_id" id="input-user_id" type="hidden" value="{{Auth::id()}}" />
                                                <input class="form-control" name="name" id="input-name" type="hidden" value="voip" />
                                                <input class="form-control{{ $errors->has('user_name') ? ' is-invalid' : '' }}" name="user_name" id="input-user_name" type="text" placeholder="{{ __('Login') }}" required="true" aria-required="true" autocomplete="off" />
                                                @if ($errors->has('heading'))
                                                <span id="user_name-error" class="error text-danger" for="input-user_name">{{ $errors->first('user_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="input-password" type="password" placeholder="{{ __('Password') }}" required="true" aria-required="true" autocomplete="off" />
                                                @if ($errors->has('password'))
                                                <span id="password-error" class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            @php
                                            echo Auth::user()->name;
                                            @endphp
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('port') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('port') ? ' is-invalid' : '' }}" name="port" id="input-port" type="text" placeholder="{{ __('code') }}" required="true" aria-required="true" autocomplete="off" />
                                                @if ($errors->has('port'))
                                                <span id="port-error" class="error text-danger" for="input-port">{{ $errors->first('port') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group{{ $errors->has('script_path') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('script_path') ? ' is-invalid' : '' }}" name="script_path" id="input-script_path" type="text" placeholder="{{ __('script path') }}" required="true" aria-required="true" autocomplete="off" />
                                                @if ($errors->has('script_path'))
                                                <span id="script_path-error" class="error text-danger" for="input-script_path">{{ $errors->first('script_path') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer ml-auto mr-auto">
                                        <button type="submit" class="btn btn-md btn-warning">{{ __('SAVE') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection