@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'login', 'title' => __('LeadsRouter')])

@section('content')<style>
  .login-page {
    background-color: #fff !important;
    background-image: url('{{ asset('material') }}/img/bg.jpeg');
    height: 90%;
    background-repeat: no-repeat;
    align-items: center;
    background-position: top center;
    filter: blur(8px);
  }
  .blurpage{
  color: white;
  font-weight: bold;
  position: absolute;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 100%;
  /* padding: 20px; */
  text-align: center;
  }

</style>
  <div class="container blurpage" style="height: auto; padding-top:55px;">
  <div class="row align-items-center">
    <div class="col-md-9 ml-auto mr-auto mb-3 text-center">
      <!-- <h3>{{ __('Log in to see how you can speed up your web development with out of the box CRUD for #User Management and more.') }} </h3> -->
    </div>
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('login') }}">
        @csrf

        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            LOGIN WITH EMAIL
          </div>
          <div class="card-body">
            <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">email</i>
                  </span>
                </div>
                <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" required>
              </div>
              @if ($errors->has('email'))
              <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                <strong>{{ $errors->first('email') }}</strong>
              </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password...') }}" required>
              </div>
              @if ($errors->has('password'))
              <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                <strong>{{ $errors->first('password') }}</strong>
              </div>
              @endif
            </div>
            <div class="form-check mr-auto ml-3 mt-3" style="display: none;">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember me') }}
                <span class="form-check-sign">
                  <span class="check"></span>
                </span>
              </label>
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary">{{ __('LOGIN') }}</button>
          </div>
        </div>
      </form>
      <div class="row">
        <div class="col-6">
          @if (Route::has('password.request'))
          <a href="{{ route('password.request') }}" class="text-light">
            <small>{{ __('Forgot password?') }}</small>
          </a>
          @endif
        </div>
        <div class="col-6 text-right">
          <a href="{{ route('register') }}" class="text-light">
            <small>Don't have LeadsRouter yet? {{ __('Create new account') }}</small>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection