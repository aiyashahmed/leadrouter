@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'login', 'title' => __('LeadsRouter')])

@section('content')
<style>
  .login-page {
    background-color: #fff !important;
    background-image: url('{{ asset('material') }}/img/bg.jpeg');
    height: 90%;
    background-repeat: no-repeat;
    align-items: center;
    background-position: top center;
    filter: blur(8px);
  }

  .blurpage {
    color: white;
    font-weight: bold;
    position: absolute;
    top: 40%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    width: 100%;
    /* padding: 20px; */
    text-align: center;
  }
</style>
<div class="container blurpage" style="height: auto; padding-top:55px;">
  <div class="row align-items-center">
    <div class="col-md-9 ml-auto mr-auto mb-3 text-center">
      <!-- <h3>{{ __('Log in to see how you can speed up your web development with out of the box CRUD for #User Management and more.') }} </h3> -->
    </div>
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="card card-login card-hidden mb-3">
          <div class="card-body">
            LOGIN
            <div class="bmd-form-group mt-3">
              <a href="{{route('login.facebook')}}" class="btn-just-icon btn-white">
                <button type="button" class="btn btn-lg btn-fb" style="background-color:#0468bb">
                  <i class="fa fa-facebook-square" style="font-size: 33px;"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login with Facebook&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </button>
              </a>
            </div>
            <div class="bmd-form-group mt-3 ">
              <a href="{{route('login.google')}}">
                <button type="button" class="btn btn-lg btn-fb" style="background-color:#DB4A39">
                  <i class="fa fa-google" style="font-size: 33px;"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login with Gmail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </button>
              </a>
            </div>
            <div class="bmd-form-group mt-3 ">
              <a href="{{route('login')}}">
                <button type="button" class="btn btn-lg btn-fb" style="background-color:#484647">
                  <i class="fa fa-envelope" style="font-size: 33px;"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login with Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </button>
              </a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection