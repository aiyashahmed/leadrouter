@extends('layouts.app', ['activePage' => 'leads_view', 'titlePage' => __('Leads View')])

@section('content')
@php
use Carbon\Carbon;
@endphp
<style>
    .counter.counter-lg {
        top: -24px !important;
    }

    .dtHorizontalExampleWrapper {
        max-width: 600px;
        margin: 0 auto;
    }

    #dtHorizontalExample th,
    td {
        white-space: nowrap;
    }

    table.dataTable thead .sorting:after,
    table.dataTable thead .sorting:before,
    table.dataTable thead .sorting_asc:after,
    table.dataTable thead .sorting_asc:before,
    table.dataTable thead .sorting_asc_disabled:after,
    table.dataTable thead .sorting_asc_disabled:before,
    table.dataTable thead .sorting_desc:after,
    table.dataTable thead .sorting_desc:before,
    table.dataTable thead .sorting_desc_disabled:after,
    table.dataTable thead .sorting_desc_disabled:before {
        bottom: .5em;
    }

    .card {
        -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, 0);
        -moz-box-shadow: 0 5px 15px rgba(0, 0, 0, 0);
        -o-box-shadow: 0 5px 15px rgba(0, 0, 0, 0);
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0);
    }

    .leadview-box {
        table-layout: fixed;
        overflow: hidden;
    }

    .leadview-box td {
        border: none;
        text-overflow: ellipsis;
    }

    .modal-backdrop {
        z-index: 0 !important;
    }

    .card .card-header {
        z-index: 0 !important;
    }

    /*    custom scrollbar */
    /* width */
    ::-webkit-scrollbar {
        width: 5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px white;
        border-radius: 10px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: white;
        border-radius: 10px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: ghostwhite;
    }
</style>
<nav class="navbar navbar-expand-lg">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="page-heading">
                <a href="#">LEADS</a>
            </div>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <form class="navbar-form">
                        <div class="input-group no-border">
                            <button type="submit" class="btn btn-white btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                            &nbsp;<input type="text" id="leadsearch" value="" class="form-control" placeholder="Search and filter" style="width:450px !important;">
                        </div>
                    </form>
                </li>
                <li class="nav-item" style="border-right: 1px solid #D5D8DC;">
                    <a class="nav-link" href="{{ route('home') }}">
                        leads :
                        <span class="counter counter-lg" style="padding:2px; top:-5px !important; border-radius:10px">{{$leadCount}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#importModal">
                        <i class="material-icons">play_for_work</i>
                        IMPORT
                    </a>
                </li>
                <!-- import Modal -->
                <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="importModalLabel">IMPORT LEADS</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <!-- <form method="POST" id="leads_import_form" enctype="multipart/form-data"> -->
                            <form action="{{ route('lead/import') }}" method="POST" id="leads_import_form" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <input type="file" name="import_file" id="import_file" class="form-control">
                                </div>
                                <div class="modal-footer">
                                    <a href="{{ asset('material') }}/leads.csv" download>
                                        <button type="button" class="btn btn-secondary">GET A SAMPLE FILE</button>
                                    </a>
                                    <button class="btn btn-success"><i class="material-icons">play_for_work</i>
                                        SUBMIT
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('export') }}">
                        <i class="material-icons">upgrade</i>
                        EXPORT
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('leadspipeline') }}">
                        <button type="button" class="btn btn-md" style="background-color:#fff; color:#000">
                            SETUP
                        </button>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('lead_create') }}">
                        <button type="button" class="btn btn-md btn-warning">
                            + NEW LEAD
                        </button>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="content" style="margin-top: 15px;">
    <div class="row" style="height:500px;overflow-x: auto; overflow-y: auto">
        <div class="col-md-12">
            <table id="leadsTable" class="table table-borderless table-sm" cellspacing="0">
                <tr>
                    @foreach($pipeline as $data)
                    <td>
                        <div class="card" style="width:300px; border:1px solid #FFF; margin:10px 1px !important;">
                            <div class="card-header" style="border-bottom:3px solid {{ $data->color }}; color:#000; padding:10px 0px 0px 0px;z-index:0;">
                                <h4 class="card-title" style="font-size:12px; font-weight: 900; text-align:center;  text-transform: uppercase;"> {{ $data->heading }}</h4>
                                <p class="card-title" style="font-size:10px; font-weight: 900; text-align:center;  text-transform: uppercase;">
                                    total leads :
                                    <span class="lead-count-{{$data->id}}">{{$row = DB::table('leads')->where('lead_step', '=', $data->id)->get()->count()}}</span>
                                </p>
                            </div>
                            <div class="card-body" style="padding:0px 10px !important;">
                                <ul class="draggable list-unstyled list-group list-group-sortable-connected-1" id="{{$data->id}}">
                                    @php
                                    $row = DB::table('leads')->select('*')
                                    ->where('lead_step', '=', $data->id)->orderBy('order','asc')->get()->all();
                                    @endphp
                                    <li style="color: white">.</li>
                                    @foreach ($row as $leads)
                                    <li class="draggable-li" data-lead="{{ $leads->id}}" id="lead-{{ $leads->id}}">
                                        <a href="{{ route('lead_edit', ['id' => $leads->id]) }}">
                                            <div class="alert alert-with-icon" data-notify="container" style="color:#000; background-color:#FFF; padding:5px 10px 0px 10px; border:1px solid #EAECEE; box-shadow: 0px 1px 5px -1px {{$data->color}}; height:80px">
                                                <div>
                                                    <div class="row">
                                                        <div class="col-2">
                                                            <img height="35px" alt="profile" src="{{ asset('material') }}/img/faces/avatar.png" class="rounded">
                                                            <br>
                                                            <span style="font-size: 9px;" class="text-primary">
                                                                Source
                                                            </span>
                                                        </div>
                                                        <div class="col-6" style="font-size: 11px;">
                                                            {{$leads->name}} <br>
{{--                                                            {{$leads->subject}}--}}
                                                        </div>
                                                        <div class="col-4" style="font-size: 11px;text-align:right">
                                                            LEAD ID: {{$leads->id}}
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-4" style="font-size: 14px;">
                                                            <span style="font-size:11px;">
                                                                {{$leads->p_contact}}
                                                            </span>
                                                        </div>
                                                        <div class="col-8" style="font-size: 11px; float:right; text-align:right">
                                                            {{$leads->created_at}}
{{--                                                            {{Carbon::create($leads->created_at)->format('H:i d/m/Y')}}--}}
                                                        </div>
                                                    </div>
                                                    <!-- <tr>
                          <td colspan="2">
                            <span style="font-size:11px; text-transform: uppercase;">
                              * {{$leads->subject}}
                                                                what type data are show this space
                                                              </span>
                                                            </td>
                                                          </tr> -->
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </td>
                    @endforeach
                </tr>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $(".ps-scrollbar-y-rail").remove();
            $(".ps-scrollbar-x-rail").remove();

            $('.draggable').sortable({
                placeholderClass: 'draggable-li',
                connectWith: '.connected'
            });

            $('.dataTables_length').addClass('bs-select');
        });
    </script>
    @endsection
