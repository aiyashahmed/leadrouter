@extends('layouts.app', ['activePage' => 'leads_view', 'titlePage' => __('Leads View')])

@section('content')
<style>
    .sidenav {
        height: 100%;
        width: 450px;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #F2F4F4;
        overflow-x: hidden;
        margin-left: 70px;
        margin-top: 67px;
    }

    .main {
        margin-left: 450px;
    }
    .form-row{
        margin-top: 7px;
    }
    input, select, textarea{
    color: #ff0000;
}
</style>
<nav class="navbar navbar-expand-lg">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="page-heading">
                <a href="#">NEW LEAD</a>
            </div>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('leads_view') }}">
                        <button type="button" class="btn btn-md" style="background-color:#fff; color:#ff9800; letter-spacing:1px">
                            <b>X</b> CLOSE
                        </button>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="content" style="margin-top: 45px;">
    <div class="container-fluid">
        <div class="row">
            <div class="sidenav" style="padding:5px; font-weight:bolder">
                <div class="row" style="margin:20px 0px 0px 10px;">
                    <form method="post" action="{{ route('lead_store') }}" autocomplete="on" class="form-horizontal">
                        @csrf
                        <div class="col-md-12">
                            @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!-- Extended material form grid -->
                            <input type="hidden" name="com_id" id="com_id" value="{{Auth::user()->act_id}}" />
                            <input type="hidden" name="user_id" id="user_id" value="{{Auth::user()->id}}" />
                            <input type="hidden" name="pipeline_id" id="pipeline_id" value="1" />
                            <input type="hidden" name="source" id="source" value="1" />
                            <input type="hidden" name="country" id="country" value="1" />
                            <!-- Grid row -->
                            <div class="form-row">
                                <!-- Grid column -->
                                <div class="col-md-12">
                                    <!-- Material input -->
                                    <div class="md-form form-group">
                                        <!-- select-->
                                        <select class="form-control{{ $errors->has('lead_step') ? ' has-danger' : '' }}" name="lead_step" id="input-lead_step" style="text-transform: uppercase;">
                                            @foreach($pipeline as $data)
                                            <option value="{{$data->id}}">{{$data->heading}}</option>
                                            @endforeach
                                        </select>
                                        <label for="input-lead_step">Lead stage</label>
                                        <!--/ select-->
                                    </div>
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-row">
                                <!-- Grid column -->
                                <div class="col-md-12">
                                    <!-- Material input -->
                                    <div class="md-form form-group{{ $errors->has('subject') ? ' has-danger' : '' }}">
                                        <input value="" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" id="input-subject" type="text" placeholder="{{ __('Subject') }}" required="true"   />
                                        <label for="input-subject">Subject</label>
                                    </div>
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-row">
                                <!-- Grid column -->
                                <div class="col-md-12">
                                    <!-- Material input -->
                                    <div class="md-form form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <input value="" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Name') }}" required="true"   />
                                        <label for="input-name">Contact Name</label>
                                    </div>
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-row">
                                <!-- Grid column -->
                                <div class="col-md-6">
                                    <!-- Material input -->
                                    <div class="md-form form-group">
                                        <input value="" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="text" placeholder="{{ __('Email') }}" required="true"   />
                                        <label for="input-email">Email</label>
                                    </div>
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                    <!-- Material input -->
                                    <div class="md-form form-group">
                                        <input value="" class="form-control{{ $errors->has('p_contact') ? ' is-invalid' : '' }}" name="p_contact" id="input-p_contact" type="tel" placeholder="{{ __('Phone') }}" required="true"   />
                                        <label for="input-p_contact">Phone</label>
                                    </div>
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-row">
                                <!-- Grid column -->
                                <div class="col-md-12">
                                    <!-- Material input -->
                                    <div class="md-form form-group">
                                        <input value="" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="position" id="input-position" type="text" placeholder="{{ __('Position') }}" required="true"   />
                                        <label for="input-position">Position</label>
                                    </div>
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-row">
                                <!-- Grid column -->
                                <div class="col-md-6">
                                    <!-- Material input -->
                                    <div class="md-form form-group">
                                        <input value="" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" name="company" id="input-company" type="text" placeholder="{{ __('Company Name') }}" required="true"   />
                                        <label for="input-company">Company</label>
                                    </div>
                                </div>
                                <!-- Grid column -->
                                <div class="col-md-6">
                                    <!-- Material input -->
                                    <div class="md-form form-group">
                                        <input value="" class="form-control{{ $errors->has('c_contact') ? ' is-invalid' : '' }}" name="c_contact" id="input-c_contact" type="text" placeholder="{{ __('Company Contact') }}" required="true"   />
                                        <label for="input-c_contact">Contact Phone</label>
                                    </div>
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-row">
                                <!-- Grid column -->
                                <div class="col-md-12">
                                    <!-- Material input -->
                                    <div class="md-form form-group">
                                        <input value="" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="input-address" type="text" placeholder="{{ __('address') }}" required="true"   />
                                        <label for="input-address">Address</label>
                                    </div>
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->

                            <button style="float: right;" type="submit" class="btn btn-warning btn-sm">Submit</button>
                            <!-- Extended material form grid -->
                        </div>
                        <div class="col-md-12"></div>
                        <div class="col-md-12"></div>
                        <div class="col-md-12"></div>
                        <div class="col-md-12"></div>
                        <div class="col-md-12"></div>
                    </form>
                </div>
            </div>
            <div class="main">
            </div>
        </div>
    </div>
</div>
@endsection
