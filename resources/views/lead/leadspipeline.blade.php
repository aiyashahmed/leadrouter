@extends('layouts.app', ['activePage' => 'leads_view', 'titlePage' => __('Leads View')])

@section('content')
    <style>
        .counter.counter-lg {
            top: -24px !important;
        }

        .sidenav {
            height: 100%;
            width: 300px;
            position: fixed;
            z-index: 2;
            top: 0;
            left: 0;
            background-color: #F8F9F9;
            overflow-x: hidden;
            padding-top: 20px;
            margin-left: 70px;
            margin-top: 67px;
            z-index: 0;
        }

        .main {
            margin-left: 300px;
            z-index: 0;
        }

        .dtHorizontalExampleWrapper {
            max-width: 600px;
            margin: 0 auto;
        }

        #lead-setup-table th,
        td {
            white-space: nowrap;
        }

        table.dataTable thead .sorting:after,
        table.dataTable thead .sorting:before,
        table.dataTable thead .sorting_asc:after,
        table.dataTable thead .sorting_asc:before,
        table.dataTable thead .sorting_asc_disabled:after,
        table.dataTable thead .sorting_asc_disabled:before,
        table.dataTable thead .sorting_desc:after,
        table.dataTable thead .sorting_desc:before,
        table.dataTable thead .sorting_desc_disabled:after,
        table.dataTable thead .sorting_desc_disabled:before {
            bottom: .5em;
        }
    </style>
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="page-heading">
                    <a href="#">LEADS SETUP</a>
                </div>
            </div>
            <div class="loading-div d-none" style="
    position: relative;
    top: 10px;
    left: 472px;
">
                <label class="badge badge-default text-dark">Please Wait..</label>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('leads_view') }}">
                            <button type="button" class="btn btn-md"
                                    style="background-color:#fff; color:#ff9800; letter-spacing:1px">
                                BACK
                            </button>
                        </a>
                    </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" href="{{ route('lead_create') }}">
                        <button type="button" class="btn btn-md btn-warning">
                            SAVE
                        </button>
                    </a>
                </li> -->
            </ul>
        </div>
    </div>
</nav>
<div class="content" style="margin-top: 15px;">
    <div class="container-fluid">
        <div class="row">
            <div class="sidenav" style="padding:5px; font-weight:500; font-size:16px; padding-left: 20px;">
                <p>LEAD SOURCES</p>
                <a class="nav-link" href="{{ route('integrate') }}">
                    <button type="button" class="btn btn-md btn-warning">
                        + Add Source
                    </button>
                </a>
            </div>
        </div>
    </nav>
    <div class="content" style="margin-top: 15px;">
        <div class="container-fluid">
            <div class="row">
                <div class="sidenav" style="padding:5px;font-weight:500;font-size:16px;padding-left: 20px;position: fixed;z-index: 10000;">
                    <p>LEAD SOURCE LIST</p>
                    <a class="nav-link" href="{{ route('integrate') }}">
                        <button type="button" class="btn btn-md btn-warning">
                            + Add Source
                        </button>
                    </a>
                </div>
                <div class="main mt-3">
                <!-- @method('put') -->
                    <div class="row">
                        <div class="col-md-6">
                            @if (session('status'))
                                <div class="alert alert-success" style="margin-top: 20px;">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>{{ session('status') }}</span>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table id="lead-setup-table" class="table table-sm" cellspacing="0" width="100%"
                                   style="border:none;">
                                <tr>
                                    @foreach($pipeline as $data)
                                        <td class="lead-added-td {{$data->step_no}}">
                                            <div class="card" style="width:250px;height: 500px;">
                                                <div class="card-header lead-title-div"
                                                     data-id="{{$data->id}}"
                                                     style="background-color:{{ $data->color }}; height:42px">
                                                    <h4 class="card-title text-white"
                                                        style="font-size:14px; text-transform: uppercase;">
{{--                                                        @if ($data->heading== "INCOMING LEADS")--}}
{{--                                                            {{ $data->heading }}--}}
{{--                                                        @else--}}
{{--                                                            <input type="text" id="{{$data->id}}"--}}
{{--                                                                   value="{{ $data->heading }}"--}}
{{--                                                                   style="color: white; background-color:{{ $data->color }}; border:none; text-transform:uppercase"--}}
{{--                                                                   onchange="changeStepName(this)">--}}
{{--                                                        @endif--}}
                                                        {{ $data->heading }}
                                                    </h4>
                                                </div>
                                                <div class="card-body">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead class=" text-primary">
                                                            <th>
                                                            </th>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>

                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
{{--                                        <td class="pipe_{{$data->id}} {{$data->step_no}}" style="display: none;">--}}
{{--                                            <div class="card" style="width:250px; height: 900px;vertical-align: top;">--}}
{{--                                                <form method="post" action="{{ route('leadspipeline_add') }}"--}}
{{--                                                      autocomplete="off" class="form-horizontal">--}}
{{--                                                    @csrf--}}
{{--                                                    <div class="card-header bg-secondary" style="height: 42px;">--}}
{{--                                                        <div--}}
{{--                                                            class="form-group{{ $errors->has('heading') ? ' has-danger' : '' }}"--}}
{{--                                                            style="margin-top:-10px; padding-top:-10px">--}}
{{--                                                            <div class="row">--}}
{{--                                                                <div class="text-left" style="float:left">--}}
{{--                                                                    <input class="form-control" name="user_id"--}}
{{--                                                                           type="hidden" value="{{Auth::id()}}"/>--}}
{{--                                                                    <input class="form-control" name="step_no"--}}
{{--                                                                           type="hidden" value="{{$data->id}}"/>--}}
{{--                                                                    <input class="form-control" name="no" type="hidden"--}}
{{--                                                                           value="{{$data->step_no}}"/>--}}
{{--                                                                    <input--}}
{{--                                                                        class="form-control{{ $errors->has('heading') ? ' is-invalid' : '' }}"--}}
{{--                                                                        name="heading" type="text"--}}
{{--                                                                        placeholder="{{ __('Title') }}" required="true"--}}
{{--                                                                        aria-required="true"--}}
{{--                                                                        style="font-size: 16px; color:#fff;  text-transform: uppercase;"/>--}}
{{--                                                                    @if ($errors->has('heading'))--}}
{{--                                                                        <span id="heading-error"--}}
{{--                                                                              class="error text-danger">{{ $errors->first('heading') }}</span>--}}
{{--                                                                    @endif--}}
{{--                                                                </div>--}}
{{--                                                                <div class="right" style="float:right">--}}
{{--                                                                    <input class="form-control color" type="text"--}}
{{--                                                                           name="color">--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="card-footer ml-auto mr-auto">--}}
{{--                                                        <button type="submit" class="btn btn-md"--}}
{{--                                                                style="background-color:#fff; color:#000">{{ __('SAVE') }}</button>--}}
{{--                                                    </div>--}}

{{--                                                </form>--}}
{{--                                            </div>--}}
{{--                                        </td>--}}
                                        <td class="plus_{{$data->id}} add-btn">
                                            <button type="button" class="btn btn-light btn-sm add-lead-setup lead-setup-{{$data->step_no}} mt-5 bg-light text-gray" data-id="{{$data->id}}" data-stepno="{{$data->step_no}}">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </td>
                                    @endforeach
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            // $('#lead-setup-table').DataTable({
            //     "scrollX": true
            // });
            $('.dataTables_length').addClass('bs-select');
        });

        function newform(id) {
            $('.pipe_' + id).show();
            $('.plus_' + id).hide();
            // alert(id);
        }

        $('#show').on('click', function () {
            $('.center').show();
            $(this).hide();
        })

        $('#close').on('click', function () {
            $('.center').hide();
            $('#show').show();
        })
    </script>
@endsection
