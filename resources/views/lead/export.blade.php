<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table tr,
        td,
        th {
            border: 1px solid #000;
        }

        table {
            border: 1px solid #000;
        }
    </style>
</head>

<body>
    @php
    $user_id = Auth::user()->id;
    $pipeline = DB::table('leadspipeline')->select('id','heading')
    ->where('user_id', '=', $user_id)
    ->orderBy('step_no', 'asc')->get();

    $arr = array();
    @endphp
    <table>
        <tr>
            @foreach($pipeline as $data)
            <th>{{$data->heading}}</th>
            @endforeach
        </tr>
        <tr></tr>
        <tr>
            @foreach($pipeline as $data)
            <td>
                @php
                $row = DB::table('leads')->select(DB::raw('CONCAT(id,";",subject,";",name,";",email,";",p_contact,";",address) as subject'))
                ->where('lead_step', '=', $data->id)
                ->orderBy('order','asc')->get()->all();
                @endphp
                @foreach ($row as $leads)
                @if($leads->subject !=="")
                <table>
                    <tr>
                        <td>{{$leads->subject}}</td>
                    </tr>
                </table>
                @endif
                @endforeach
            </td>
            @endforeach
        </tr>
    </table>

</body>

</html>