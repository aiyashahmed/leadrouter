@extends('layouts.app', ['activePage' => 'profile', 'titlePage' => __('User Profile')])

@section('content')
<style>
  .material-switch>input[type="checkbox"] {
    display: none;
  }

  .material-switch>label {
    cursor: pointer;
    height: 0px;
    position: relative;
    width: 40px;
  }

  .material-switch>label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position: absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
  }

  .material-switch>label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
  }

  .material-switch>input[type="checkbox"]:checked+label::before {
    background: inherit;
    opacity: 0.5;
  }

  .material-switch>input[type="checkbox"]:checked+label::after {
    background: inherit;
    left: 20px;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('profile.update') }}" autocomplete="off" class="form-horizontal">
          @csrf
          @method('put')
          <div class="card ">
            <div class="card-header" style="background-color: #0a1b2b; color:#fff">
              <h4 class="card-title">{{ __('Edit Profile') }}</h4>
              <p class="card-category">{{ __('User information') }}</p>
            </div>
            <div class="card-body ">
              @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>{{ session('status') }}</span>
                  </div>
                </div>
              </div>
              @endif
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Name') }}" value="{{ old('name', auth()->user()->name) }}" required="true" aria-required="true" />
                    @if ($errors->has('name'))
                    <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ __('Email') }}" value="{{ old('email', auth()->user()->email) }}" required />
                    @if ($errors->has('email'))
                    <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Contact Number') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('contact') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }}" name="contact" id="input-contact" type="tel" placeholder="{{ __('contact number') }}" value="{{ old('contact', auth()->user()->contact) }}" />
                    @if ($errors->has('contact'))
                    <span id="contact-error" class="error text-danger" for="input-contact">{{ $errors->first('contact') }}</span>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer ml-auto mr-auto">
              <button type="submit" class="btn btn-warning">{{ __('UPDATE') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header" style="background-color: #0a1b2b; color:#fff">
            <h4 class="card-title">{{ __('Integrations List') }}</h4>
            <p class="card-category">{{ __('Sources') }}</p>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-2">
                <hr>
                No integrations.
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
                <hr>
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
                <hr>
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
            </div>
          </div>
          <div class="card-footer ml-auto mr-auto">

          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header" style="background-color: #0a1b2b; color:#fff">
            <h4 class="card-title">
              <i class="material-icons" style="color:#fff">notifications_active</i></h4>
            <h4 class="card-title">
              {{ __('Notifications') }}<span class="card-category"> {{ __('Settings') }}</span></h4>

          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-2">
                <hr>
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
              <div class="col-sm-1">
                <div class="material-switch pull-right">
                  <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox" />
                  <label for="someSwitchOptionWarning" class="label-warning"></label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
                <hr>
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
              <div class="col-sm-1">
                <div class="material-switch pull-right">
                  <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox" checked/>
                  <label for="someSwitchOptionWarning" class="label-warning"></label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
                <hr>
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
              <div class="col-sm-1">
                <div class="material-switch pull-right">
                  <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox" />
                  <label for="someSwitchOptionWarning" class="label-warning"></label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
                <hr>
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
              <div class="col-sm-1">
                <div class="material-switch pull-right">
                  <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox" checked/>
                  <label for="someSwitchOptionWarning" class="label-warning"></label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
                <hr>
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
              <div class="col-sm-1">
                <div class="material-switch pull-right">
                  <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox" />
                  <label for="someSwitchOptionWarning" class="label-warning"></label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
                <hr>
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
              <div class="col-sm-1">
                <div class="material-switch pull-right">
                  <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox" />
                  <label for="someSwitchOptionWarning" class="label-warning"></label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
                <hr>
              </div>
              <div class="col-sm-7">
                <hr>
              </div>
              <div class="col-sm-1">
                <div class="material-switch pull-right">
                  <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox" />
                  <label for="someSwitchOptionWarning" class="label-warning"></label>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer ml-auto mr-auto">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection