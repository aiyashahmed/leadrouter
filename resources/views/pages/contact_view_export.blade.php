<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table tr,
        td,
        th {
            border: 1px solid #000;
        }

        table {
            border: 1px solid #000;
        }
    </style>
</head>

<body>
    <table class="table">
        <tr>
            <th>LEAD ID</th>
            <th>NAME</th>
            <th>CONTACT</th>
            <th>ADDRESS</th>
            <th>SUBJECT</th>
            <th>LEAD STATUS</th>
        </tr>
            @php

            $row = DB::table('leads')->join('leadspipeline','leadspipeline.id','leads.lead_step')
            ->select('leads.*','leadspipeline.heading')
            ->where('leadspipeline.heading','!=','INCOMING LEADS')
            ->orderBy('order','asc')->get()->all();
            @endphp
            @foreach ($row as $leads)
            <tr>
                <td>{{$leads->id}}</td>
                <td>{{$leads->name}}</td>
                <td>{{$leads->p_contact}}, {{$leads->c_contact}}</td>
                <td>{{$leads->address}}</td>
                <td>{{$leads->subject}}</td>
                <td>{{$leads->heading}}</td>
            </tr>
            @endforeach
    </table>
</body>

</html>