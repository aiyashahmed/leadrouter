@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'login', 'title' => __('LeadsRouter')])

@section('content')
<style>
    .login-page {
        background-color: #fff !important;
        background-image: url('{{ asset('material') }}/img/bg.jpeg');
        height: 90%;
        background-repeat: no-repeat;
        align-items: center;
        background-position: top center;
        filter: blur(8px);
    }

    .blurpage {
        color: white;
        font-weight: bold;
        position: absolute;
        top: 60%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 2;
        width: 100%;
        /* padding: 20px; */
        text-align: center;
        height: auto;
    }
</style>
<div class="container blurpage">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Calendly inline widget begin -->
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <div class="calendly-inline-widget" data-url="https://calendly.com/arshadrifas/30min?primary_color=56ae7e" style="min-width:320px;height:670px;"></div>
                <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
                <!-- Calendly inline widget end -->
            </div>
        </div>
    </div>
</div>
@endsection