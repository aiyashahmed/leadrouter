@extends('layouts.app', ['activePage' => 'calendar_view', 'titlePage' => __('Calendar view')])
@section('content')
<nav class="navbar navbar-expand-lg">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="page-heading">
        <a href="#">CALENDER</a>
      </div>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
      <ul class="navbar-nav">
        <!-- <li class="nav-item">
          <a class="nav-link" href="{{ route('contact_export') }}">
            <i class="material-icons">upgrade</i>
            EXPORT
          </a>
        </li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="content" style="margin-top: 55px;">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        @php
        use Carbon\Carbon;
        $today = Carbon::today();

        echo '<h4 class="w3-text-teal" style="text-transform: uppercase;">
          <center>' . $today->format('F Y') . '</center>
        </h4>';

        $tempDate = Carbon::createFromDate($today->year, $today->month, 1);



        echo '<div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr style="border-top: 1px solid #D5D8DC; height: 75px; font-weight: 900;">
                <th>Sun</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
                <th>Sat</th>
              </tr>
            </thead>';

            $skip = $tempDate->dayOfWeek;


            for($i = 0; $i < $skip; $i++) { $tempDate->subDay();
              }


              //loops through month
              do
              {
              echo '<tr style="height: 75px;">';
                //loops through each week
                for($i=0; $i < 7; $i++) { echo '<td><span class="date">' ; echo $tempDate->day;

                  echo '</span></td>';

                  $tempDate->addDay();
                  }
                  echo '</tr>';

              }while($tempDate->month == $today->month);

              echo '
          </table>';
          @endphp
        </div>
      </div>
    </div>
  </div>
</div>

@endsection