@extends('layouts.app', ['activePage' => 'accounts_view', 'titlePage' => __('Accounts')])

@section('content')
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="page-heading">
                    <select class="custom-select" id="select-email">
                        <option value="inbox">Inbox</option>
                        <option value="sent">Sent</option>
                        <option value="trash">Trash</option>
                    </select>

                </div>
                <div class="pl-1">
                    <button class="btn btn-warning" data-toggle="modal" data-target="#add-remove-accounts-modal">
                        Settings
                    </button>
                </div>
            </div>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <form id="table-search" class="navbar-form">
                            <div class="input-group no-border">
                                <button type="submit" class="btn btn-white btn-just-icon">
                                    <i class="material-icons">search</i>
                                    <div class="ripple-container"></div>
                                </button>
                                &nbsp;<input type="text" value="" class="form-control" placeholder="Search and filter"
                                             style="width:450px !important;" name="table-search">
                            </div>
                        </form>
                    </li>
                    <li class="nav-item" style="border-right: 1px solid #D5D8DC;">
                        <a class="nav-link" href="#">
                            Filtered emails | <span id="email-total-count">0</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <div class="nav-link">
                            <select class="custom-select" id="email-account-select"
                                    style="background-color:#fff; color:#000">
                                @foreach($data as $d)
                                    <option id="opt-{{$d->id}}" value="{{$d->email}}">{{$d->email}}</option>
                                @endforeach
                            </select>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="nav-link">
                            <button id="compose-page-btn" href="{{url('email/compose')}}" class="btn btn-warning">
                                <div class="row">
                                    <i class="fa fa-pencil pr-1"></i> compose
                                </div>
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                {{--                <div>--}}
                {{--                    <button class="btn btn-outline-warning btn-sm d-none" id="inbox-btn">Inbox</button>--}}
                {{--                    <button class="btn btn-outline-info btn-sm" id="sendbox-btn">Send box</button>--}}
                {{--                </div>--}}
                <div class="col-md-12">
                    <div class="card" id="inbox-div">
                        <div class="card-header card-header-warning">
                            <div class="row">
                                <div class="col-9">
                                    <h4 class="card-title ">Inbox</h4>
                                </div>
                            </div>
                            {{--            <p class="card-category"> Here is a subtitle for this table</p>--}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="email-inbox-table">
                                    <thead class=" text-primary">
                                    <th>
                                        from
                                    </th>
                                    <th>
                                        Message and Leads
                                    </th>
                                    <th>
                                        Date and Time
                                    </th>
                                    <th>
                                        Account
                                    </th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{--                    send box--}}
                    <div class="card d-none" id="sendbox-div">
                        <div class="card-header card-header-warning">
                            <div class="row">
                                <div class="col-9">
                                    <h4 class="card-title">Send Box</h4>
                                </div>
                            </div>
                            {{--            <p class="card-category"> Here is a subtitle for this table</p>--}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="email-send-box-table" style="width: 100%">
                                    <thead class="text-primary">
                                    <th>
                                        to
                                    </th>
                                    <th>
                                        Message and Leads
                                    </th>
                                    <th>
                                        Date and Time
                                    </th>
                                    <th>
                                        Account
                                    </th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{--                    trash --}}
                    <div class="card d-none" id="trashmail-div">
                        <div class="card-header card-header-warning">
                            <div class="row">
                                <div class="col-9">
                                    <h4 class="card-title">Trash</h4>
                                </div>
                            </div>
                            {{--            <p class="card-category"> Here is a subtitle for this table</p>--}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="email-trash-box-table" style="width: 100%">
                                    <thead class="text-primary">
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Message and Leads
                                    </th>
                                    <th>
                                        Date and Time
                                    </th>
                                    <th>
                                        Account
                                    </th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-remove-accounts-modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:100px">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add / Remove accounts</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <a href="{{url('email/gmail/connect')}}" class="btn btn-sm btn-outline-warning">
                                    <i class="fa fa-google" style="color: #c53025"></i>
                                </a>
                                <button class="btn btn-sm btn-outline-warning">
                                    <i class="fa fa-yahoo font-weight-bold" style="color: purple"></i>
                                    <i class="fa fa-envelope font-weight-bold" style="color: purple"></i>
                                </button>
                                <button class="btn btn-sm btn-outline-warning" id="smtp-imap-btn">
                                    <i class="fa fa-wrench text-gray"></i>
                                    <i class="fa fa-envelope text-gray"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card d-none" id="smtp-form-div">
                        <div class="card-body">
                            <div>
                                <label id="email-setting-error" class="badge text-danger"></label>
                            </div>
                            <form id="email-setting-form">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="host" placeholder="Host">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                                <div class="row">
                                    <button type="submit" class="btn btn-outline-warning btn-sm col-10" id="connect-btn">Connect
                                    </button>
                                    <button type="button" class="btn btn-default btn-sm col" id="smtp-form-cancel-btn">
                                        <i class="fa fa-window-close"></i>
                                    </button>
                                </div>

                                <div class="progress mt-2 d-none">
                                    <div class="progress-bar">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <table class="table" id="remove-accounts-table">
                                <thead>
                                <th>Accounts</th>
                                <th>Remove</th>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                    <tr id="tr-{{$d->id}}">
                                        <td>{{$d->email}}</td>
                                        <td>
                                            <button class="btn btn-sm btn-outline-warning account-remove-btn"
                                                    id="{{$d->id}}">
                                                <i class="fa fa-window-close text-danger"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
