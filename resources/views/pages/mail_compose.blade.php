@extends('layouts.app', ['activePage' => 'accounts_view', 'titlePage' => __('Accounts')])

@section('content')
    <style>
        .navbar {
            background-color: #fff;
            position: fixed;
            top: 0;
            left: 91px;
            right: 0;
            border-bottom: 1px solid #e8eaeb;
            box-sizing: border-box;
            height: 66px !important;
            z-index: 9999;
        }
    </style>
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="page-heading">
                    <a href="{{url('accounts_view')}}" class="btn btn-success">
                        <i class="fa fa-arrow-left pr-2"></i>
                        Back</a>
                </div>
            </div>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <div class="nav-link">
                            <select class="custom-select" id="email-account-select" style="background-color:#fff; color:#000">
                                @foreach($data as $d)
                                    <option value="{{$d->email}}">{{$d->email}}</option>
                                @endforeach
                            </select>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="nav-link" href="#">
                            <button class="btn btn-warning" id="mail-send">
                                <div class="row">
                                    <i class="fa fa-telegram pr-1"></i> Send
                                </div>
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <label class="badge text-danger text-center" id="error-div"></label>
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        <form id="compose-mail-form">
                            <div class="form-group row">
                                <label class="col-2">
                                    To
                                </label>
                                <div class="col">
                                    <input type="email" class="form-control" name="to">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2">
                                    Subject
                                </label>
                                <div class="col">
                                    <input type="text" class="form-control" name="subject">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-2">
                                    Message
                                </label>
                                <div class="col">
                                    <textarea id="txtEditor"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
