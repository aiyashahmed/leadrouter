@extends('layouts.app', ['activePage' => 'integrate', 'titlePage' => __('Integrate')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon" >
                <i class="material-icons">call</i>
              </div>
              <p class="card-category">Call Tracking</p>
              <a aria-disabled="true" class="btn btn-round btn-fill btn-info">Integrate</a>
            </div>
            <div class="card-footer">
              <div class="stats">
                Voip Integration
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                <i class="material-icons">mail_outline</i>
              </div>
              <p class="card-category">Mail</p>
              <a aria-disabled="true" class="btn btn-round btn-fill btn-info">Integrate</a>
            </div>
            <div class="card-footer">
              <div class="stats">
                Mail Integration
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
              <i class="fa fa-facebook"></i>
              </div>
              <p class="card-category">Like page</p>
              <a aria-disabled="true" class="btn btn-round btn-fill btn-info">Integrate</a>
            </div>
            <div class="card-footer">
              <div class="stats">
                Facebook Integration
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="fa fa-whatsapp"></i>
              </div>
              <p class="card-category">Bussiness</p>
              <a aria-disabled="true" class="btn btn-round btn-fill btn-info">Integrate</a>
            </div>
            <div class="card-footer">
              <div class="stats">
                WhatsApp Integration
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();
    });
  </script>
@endpush