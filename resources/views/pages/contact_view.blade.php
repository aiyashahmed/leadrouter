@extends('layouts.app', ['activePage' => 'contact_view', 'titlePage' => __('Contact List')])

@section('content')
<style>
  table td {
    text-transform: capitalize;
  }
</style>
<nav class="navbar navbar-expand-lg">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="page-heading">
        <a href="#">CONTACTS</a>
      </div>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="{{ route('contact_export') }}">
            <i class="material-icons">upgrade</i>
            EXPORT
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="content" style="margin-top: 55px;">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table" cellspacing="0" width="100%" id="contact_list1">
            <thead style="background-color: #0a1b2b; border:#0a1b2b; color:#a9afbb;text-transform: uppercase;">
              <th>LEAD ID</th>
              <th>Name</th>
              <th>Contact</th>
              <th>Address</th>
              <th>Subject</th>
              <th>Lead Status</th>
            </thead>
            <tbody>
              @php

              $row = DB::table('leads')->join('leadspipeline','leadspipeline.id','leads.lead_step')
              ->select('leads.*','leadspipeline.heading')
              ->where('leadspipeline.heading','!=','INCOMING LEADS')
              ->orderBy('order','asc')->get()->all();
              @endphp
              @foreach ($row as $leads)
              <tr>
                <td>{{$leads->id}}</td>
                <td>{{$leads->name}} </td>
                <td>{{$leads->p_contact}}, {{$leads->c_contact}}</td>
                <td>{{$leads->address}}</td>
                <td>{{$leads->subject}}</td>
                <td>{{$leads->heading}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function() {
      $('#contact_list').DataTable();
    });
  </script>
  @endsection