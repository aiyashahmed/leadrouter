<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Laravel\Socialite\Facades\Socialite;
use Google_Client;
use Google_Service_Gmail;

// use App\Http\Controllers\Controller\Auth;
// use Illuminate\Support\Facades\Auth;
use Exception;
use App\Models\User;
use App\Models\GmailCredential;
use Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // login with facebook
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {
        $whichUser = 2;
        $user = Socialite::driver('facebook')->user();
        $this->_registerOrLoginUser($user, $whichUser);

        return redirect()->route('home');
    }

    // login with google
    public function redirectToGoogle()
    {
        return Socialite::driver('google')
            ->with(["prompt" => "select_account"])
//            ->scopes(['openid', 'profile', 'email', Google_Service_Gmail::GMAIL_READONLY])
            ->redirect();
    }

    public function handleGoogleCallback()
    {
        $whichUser = 1;
        $user = Socialite::driver('google')->user();
        $this->_registerOrLoginUser($user, $whichUser);
        return redirect()->route('home');
    }

    protected function _registerOrLoginUser($data, $whichUser)
    {
        $user = User::where('email', '=', $data->email)->first();

        if (!$user) {
            $user = new User();
            $user->name = $data->name;
            $user->email = $data->email;
            $user->provider_id = $data->id;
            $user->avatar = $data->avatar;
            $user->which_user = $whichUser;
            $user->email_verified_at = now();
            $user->save();
        }

//        GmailCredential::newCredentials($data, $user->id);

        Auth::Login($user);
    }
}
