<?php

namespace App\Http\Controllers;

use Google\Exception;
use Illuminate\Http\Request;
use Google_Client;
use Google_Service_Gmail;
use App\Models\Email;
use App\Models\Lead;
use App\Models\GmailCredential;
use Auth;
use DataTables;
use Laravel\Socialite\Facades\Socialite;
use Mail;
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Message;

//use Webklex\PHPIMAP\Client;

class EmailController extends Controller
{

    public function showEmails()
    {
        $userId = Auth::user()->id;

        $data = GmailCredential::getUserData($userId);

        return view('pages.accounts_view', compact('data'));
    }

    public function getInbox()
    {
        $user_id = Auth::user()->id;
        $data = Email::getInbox($user_id);
        return Datatables::of($data)->make(true);
    }

    public function getSendbox()
    {
        $user_id = Auth::user()->id;
        $data = Email::sendbox($user_id);
        return Datatables::of($data)->make(true);
    }

    public function getTrash()
    {
        $user_id = Auth::user()->id;
        $data = Email::getTrash($user_id);
        return Datatables::of($data)->make(true);
    }

    public function connectHotMail(Request $request)
    {
        set_time_limit(0);
        //        check mail host, username and password.

        //        $inbox = imap_open("{mail.triecode.com.au:993/ssl}", "aiyash@triecode.com.au", "Aiyash@123")

        $host = "{" . $request->host . ":993/ssl}";
        $email = $request->email;
        $password = $request->password;
        $user_id = Auth::user()->id;
        $connection = true;
        $success = false;
        $today = date("d M Y", time() + 86400);

        try {
            $inbox = imap_open($host, $email, $password);
        } catch (\Exception $e) {
            $connection = false;
        }

        //        $list = imap_list($inbox, $host, '*');
        //        print_r($list);

        if ($connection) {
            //            verify email account
            $existAccount = Email::verifyAccount($host, $email, $password);

            if (!$existAccount) Email::insertIntoEmailConfig($user_id, $host, $email, $password);

            //                insert inbox email
            $insertInbox = $this->saveHotmailInbox($user_id, $host, $email, $password, $today);

            //                inserting sent email
            $host = $host . "INBOX.Sent";
            $insertInbox = $this->saveSentHotMail($user_id, $host, $email, $password, $today);

            //            $host = $host . "INBOX.Trash";
            $insertInbox = $this->saveHotmailTrash($user_id, $host, $email, $password, $today);

            $success = true;
        }

        if ($success) {
            $data = Email::getAccountDataByEmail($email);
            return response([
                'success' => $success,
                'email' => $data->email,
                'id' => $data->id
            ]);
        }

        return response([
            'success' => $success,
        ]);
    }

    private function saveHotmailInbox($user_id, $host, $email, $password, $today)
    {
        $error = false;
        $inbox = imap_open($host, $email, $password);
        $emails = imap_search($inbox, 'BEFORE "' . $today . '"');

        //        if (!$emails) $emails = imap_search($inbox, 'SINCE "' . $today . '"');

        if (!$emails) return true;


        rsort($emails);

        $account_id = Email::getAccountId($email);


        $i = 0;
        //        save only 100 emails
        foreach ($emails as $emailNo) {
            $i++;

            $overview = imap_fetch_overview($inbox, $emailNo, 0);

            $header = imap_header($inbox, $emailNo);

            //            insert into email
            if (isset($overview[0]->subject)) {
                $subject = $overview[0]->subject;
                $subject = imap_utf8($overview[0]->subject);
            }
            $msg_no = $overview[0]->msgno;
            $body = imap_fetchbody($inbox, $emailNo, 2);
            //            $body = quoted_printable_decode($body);

            $insertIntoEmail = Email::insertIntoEmail($user_id, $account_id, $msg_no, $subject, "");
            $emailId = $insertIntoEmail;
            //
            //
            //            insert into email_receive
            $name = imap_utf8($overview[0]->from);
            $name = strtok($name, '<');

            $from = $header->from[0]->mailbox . "@" . $header->from[0]->host;

            $date = $overview[0]->date;

            $insertIntoEmailReceive = Email::insertIntoEmailReceive($emailId, $from, $name, $date);

            $insertLeads = Lead::addEmailLeads($subject, $from, $name, $user_id, $date);

            if ($insertIntoEmail && $insertIntoEmailReceive && $insertLeads) {
                $error = true;
            }

            if ($i == 100) {
                return $error;
            }
        }
    }

    private function saveSentHotMail($user_id, $host, $email, $password, $today)
    {
        $error = false;
        $inbox = imap_open($host, $email, $password);
        $emails = imap_search($inbox, 'BEFORE "' . $today . '"');

        if (!$emails) return true;

        rsort($emails);

        $account_id = Email::getAccountId($email);

        $i = 0;
        //        save only 100 emails
        foreach ($emails as $emailNo) {
            $i++;

            $overview = imap_fetch_overview($inbox, $emailNo, 0);

            $header = imap_header($inbox, $emailNo);

            //            insert into email
            if (isset($overview[0]->subject)) {
                $subject = $overview[0]->subject;
                $subject = imap_utf8($overview[0]->subject);
            }

            $msg_no = $overview[0]->msgno;
            //            $body = imap_fetchbody($inbox, $emailNo, 2);
            //            $body = quoted_printable_decode($body);

            $insertIntoEmail = Email::insertIntoEmail($user_id, $account_id, $msg_no, $subject, "");
            $emailId = $insertIntoEmail;
            //
            //
            //            insert into email_receive
            $name = $overview[0]->to;

            $to = $header->to[0]->mailbox . "@" . $header->to[0]->host;

            $date = $overview[0]->date;

            $insertIntoEmailSend = Email::insertIntoEmailSend($emailId, $to, $name, $date);

            $insertLeads = Lead::addEmailLeads($subject, $to, $name, $user_id, $date);

            if ($insertIntoEmail && $insertIntoEmailSend && $insertLeads) {
                $error = true;
            }

            if ($i == 100) {
                return $error;
            }
        }
    }

    private function saveHotmailTrash($user_id, $host, $email, $password, $today)
    {
        $error = false;
        $inbox = imap_open($host, $email, $password);
        $emails = imap_search($inbox, 'BEFORE "' . $today . '"');

        if (!$emails) return true;

        rsort($emails);

        $account_id = Email::getAccountId($email);


        $i = 0;
        //        save only 100 emails
        foreach ($emails as $emailNo) {
            $i++;

            $overview = imap_fetch_overview($inbox, $emailNo, 0);

            $header = imap_header($inbox, $emailNo);

            //            insert into email
            if (isset($overview[0]->subject)) {
                $subject = $overview[0]->subject;
                $subject = imap_utf8($overview[0]->subject);
            }
            $msg_no = $overview[0]->msgno;
            $body = imap_fetchbody($inbox, $emailNo, 2);
            //            $body = quoted_printable_decode($body);

            $insertIntoEmail = Email::insertIntoEmail($user_id, $account_id, $msg_no, $subject, "");
            $emailId = $insertIntoEmail;
            //
            //
            //            insert into email_receive
            $name = imap_utf8($overview[0]->from);
            $name = strtok($name, '<');

            $from = $header->from[0]->mailbox . "@" . $header->from[0]->host;

            $date = $overview[0]->date;

            $insertIntoEmailTrash = Email::insertIntoEmailTrash($emailId, $from, $name, $date);

            $insertLeads = Lead::addEmailLeads($subject, $from, $name, $user_id, $date);

            if ($insertIntoEmail && $insertIntoEmailTrash && $insertLeads) {
                $error = true;
            }

            if ($i == 100) {
                return $error;
            }
        }
    }

    public function connectGmail()
    {
        return Socialite::driver('google')
            ->with(["prompt" => "select_account"])
            ->scopes(['openid', 'profile', 'email', Google_Service_Gmail::GMAIL_READONLY])
            ->redirect();
    }

    public function handleGmailCallback()
    {
        $data = Socialite::driver('google')->user();

        //        insert users details to user_gmail_credentials
        GmailCredential::newCredentials($data, Auth::user()->id);

        $insertEmail = $this->extractGmailInbox($data);

        if ($insertEmail) return redirect('/integrate_voip/email');
    }

    public function extractGmailInbox($userData)
    {

        $userId = Auth::user()->id;

        $connect = false;

        if (empty($userData)) {
            $datas = GmailCredential::where('user_id', $userId)->get();

            foreach ($datas as $data) {
                $google_client_token = [
                    'access_token' => $data->access_token,
                    'refresh_token' => $data->refresh_token,
                    'expires_in' => $data->expires_in
                ];

                $account_id = Email::getAccountId($data->email);

                $client = new Google_Client();
                $client->setApplicationName("leadrouter");
                $client->setDeveloperKey(env('GOOGLE_SERVER_KEY'));
                $client->setAccessToken(json_encode($google_client_token));

                $optParams = [];
                $optParams['maxResults'] = 100; // Return Only 10 Messages

                try {
                    $service = new Google_Service_Gmail($client);

                    $optParams['labelIds'] = 'INBOX'; // Only show messages in Inbox
                    $inbox = $this->insertGmailInboxMail($service, $optParams, $userId, $account_id);

                    $optParams['labelIds'] = 'SENT'; // Only show messages in SENTBOX
                    $sent = $this->insertGmailSentMail($service, $optParams, $userId, $account_id);

                    $optParams['labelIds'] = 'TRASH'; // Only show messages in TRASH messages
                    $trash = $this->insertGmailTrashMail($service, $optParams, $userId, $account_id);

                    if ($inbox && $sent && $trash) {
                        $connect = true;
                    }
                } catch (Exception $e) {
                    $connect = 2;
                }
            }

            return response([
                'connect' => $connect
            ]);
        } else {

            $google_client_token = [
                'access_token' => $userData->token,
                'refresh_token' => $userData->refreshToken,
                'expires_in' => $userData->expiresIn
            ];

            $data = GmailCredential::where('user_id', $userId)->where('google_user_id', $userData->id)->first();

            $account_id = Email::getAccountId($data->email);

            $client = new Google_Client();
            $client->setApplicationName("leadrouter");
            $client->setDeveloperKey(env('GOOGLE_SERVER_KEY'));
            $client->setAccessToken(json_encode($google_client_token));

            $optParams = [];
            $optParams['maxResults'] = 100; // Return Only 10 Messages

            try {
                $service = new Google_Service_Gmail($client);

                $optParams['labelIds'] = 'INBOX'; // Only show messages in Inbox
                $inbox = $this->insertGmailInboxMail($service, $optParams, $userId, $account_id);

                $optParams['labelIds'] = 'SENT'; // Only show messages in SENTBOX
                $sent = $this->insertGmailSentMail($service, $optParams, $userId, $account_id);

                $optParams['labelIds'] = 'TRASH'; // Only show messages in TRASH messages
                $trash = $this->insertGmailTrashMail($service, $optParams, $userId, $account_id);


                if ($inbox && $sent && $trash) {
                    $connect = true;
                }
            } catch (Exception $e) {
                $connect = 2;
            }

            return response([
                'connect' => $connect
            ]);
        }
    }

    private function insertGmailInboxMail($service, $optParams, $userId, $accountId)
    {
        $messages = $service->users_messages->listUsersMessages('me', $optParams);
        $messageLists = $messages->getMessages();
        foreach ($messageLists as $mList) {
            $messageId = $mList->getId();
            $optParamsGet['format'] = 'full'; // Display message in payload
            $message = $service->users_messages->get('me', $messageId, $optParamsGet);
            $headers = $message->getPayload()->getHeaders();
            $snippet = $message->getSnippet();

            foreach ($headers as $single) {

                if ($single->getName() == 'Subject') {

                    $messageSubject = $single->getValue();
                } else if ($single->getName() == 'Date') {

                    $messageDate = $single->getValue();
                    $messageDate = date('M jS Y h:i A', strtotime($messageDate));
                } else if ($single->getName() == 'From') {

                    $messageSender = $single->getValue();

                    $address_array = imap_rfc822_parse_adrlist($messageSender, "localhost");
                    if (!is_array($address_array) || count($address_array) < 1)
                        return FALSE;
                    $from = $address_array[0]->mailbox . '@' . $address_array[0]->host;

                    $messageSender = str_replace('"', '', $messageSender);

                    $messageSender = substr($messageSender, 0, strpos($messageSender, "<"));
                }
            }

            $existEmail = Email::emailExist($messageId, $userId);
            if (!$existEmail) {
                $insertMail = Email::insertIntoEmail($userId, $accountId, $messageId, $messageSubject, $snippet);
                $emailId = $insertMail;

                Lead::addEmailLeads($messageSubject, $from, $messageSender, Auth::user()->id,$messageDate);

                Email::insertIntoEmailReceive($emailId, $from, $messageSender, $messageDate);
            }
        }

        return true;
    }

    private function insertGmailSentMail($service, $optParams, $userId, $accountId)
    {
        $messages = $service->users_messages->listUsersMessages('me', $optParams);
        $messageLists = $messages->getMessages();
        foreach ($messageLists as $mList) {
            $messageId = $mList->getId();
            $optParamsGet['format'] = 'full'; // Display message in payload
            $message = $service->users_messages->get('me', $messageId, $optParamsGet);
            $headers = $message->getPayload()->getHeaders();
            $snippet = $message->getSnippet();

            foreach ($headers as $single) {

                if ($single->getName() == 'Subject') {

                    $messageSubject = $single->getValue();
                } else if ($single->getName() == 'Date') {

                    $messageDate = $single->getValue();
                    $messageDate = date('M jS Y h:i A', strtotime($messageDate));
                } else if ($single->getName() == 'To') {

                    $messageReceiver = $single->getValue();

                    $address_array = imap_rfc822_parse_adrlist($messageReceiver, "localhost");
                    if (!is_array($address_array) || count($address_array) < 1)
                        return FALSE;

                    $to = $address_array[0]->mailbox . '@' . $address_array[0]->host;

                    $messageReceiver = str_replace('"', '', $messageReceiver);

                    $messageReceiver = substr($messageReceiver, 0, strpos($messageReceiver, "<"));

                    if (!$messageReceiver) $messageReceiver = $to;
                }
            }

            $existEmail = Email::emailExist($messageId, $userId);
            if (!$existEmail) {
                $insertMail = Email::insertIntoEmail($userId, $accountId, $messageId, $messageSubject, $snippet);
                $emailId = $insertMail;

                Lead::addEmailLeads($messageSubject, $to, $messageReceiver, Auth::user()->id,$messageDate);

                Email::insertIntoEmailSend($emailId, $to, $messageReceiver, $messageDate);
            }
        }

        return true;
    }

    private function insertGmailTrashMail($service, $optParams, $userId, $accountId)
    {
        $messages = $service->users_messages->listUsersMessages('me', $optParams);
        $messageLists = $messages->getMessages();
        foreach ($messageLists as $mList) {
            $messageId = $mList->getId();
            $optParamsGet['format'] = 'full'; // Display message in payload
            $message = $service->users_messages->get('me', $messageId, $optParamsGet);
            $headers = $message->getPayload()->getHeaders();
            $snippet = $message->getSnippet();

            foreach ($headers as $single) {

                if ($single->getName() == 'Subject') {

                    $messageSubject = $single->getValue();
                } else if ($single->getName() == 'Date') {

                    $messageDate = $single->getValue();
                    $messageDate = date('M jS Y h:i A', strtotime($messageDate));
                } else if ($single->getName() == 'From') {

                    $messageReceiver = $single->getValue();

                    $address_array = imap_rfc822_parse_adrlist($messageReceiver, "localhost");
                    if (!is_array($address_array) || count($address_array) < 1)
                        return FALSE;

                    $from = $address_array[0]->mailbox . '@' . $address_array[0]->host;

                    $messageReceiver = str_replace('"', '', $messageReceiver);

                    $messageReceiver = substr($messageReceiver, 0, strpos($messageReceiver, "<"));

                    if (!$messageReceiver) $messageReceiver = $from;
                }
            }

            $existEmail = Email::emailExist($messageId, $userId);

            if (!$existEmail) {
                $insertMail = Email::insertIntoEmail($userId, $accountId, $messageId, $messageSubject, $snippet);
                $emailId = $insertMail;

                Lead::addEmailLeads($messageSubject, $from, $messageReceiver, Auth::user()->id, $messageDate);

                Email::insertIntoEmailTrash($emailId, $from, $messageReceiver, $messageDate);
            }
        }

        return true;
    }

    public function connectYahoo()
    {
        return Socialite::driver('yahoo')
            ->scopes(['mail-r'])
            ->redirect();
        die;
        //        $inbox = imap_open("{imap.gmail.com:993/ssl}", "aiyashahmed96@gmail.com", "Shamhaiiyash#43");
        $inbox = imap_open("{imap.mail.yahoo.com:993/ssl}", "triecodecrm@yahoo.com", "Trie@891");

        //        $host = "{" . $request->host . ":993/ssl}";
        //        $email = $request->email;
        //        $password = $request->password;
        //        $user_id = Auth::user()->id;
        //        $connection = true;
        //        $success = false;
        //        $today = date("d M Y", time() + 86400);


        $list = imap_list($inbox, "{imap.mail.yahoo.com:993/ssl}", '*');
        print_r($list);
    }

    public function handleYahooCallback()
    {
        $data = Socialite::driver('yahoo')->user();
        print_r($data);
    }

    public function compose()
    {
        $data = GmailCredential::getUserData(Auth::user()->id);

        return view('pages.mail_compose', compact('data'));
    }

    public function sendMail(Request $request)
    {
        $success = false;

        $to = $request->to;
        $from = $request->from;
        $subject = $request->subject;
        $body = $request->message;
        $date = date('M jS Y h:i A');
        $receiverName = substr($to, 0, strrpos($to, '@'));

        $sender = Auth::user()->name;
        $userId = Auth::user()->id;
        $accountId = Email::getAccountId($from);

        $transport = (new Swift_SmtpTransport(config('mail.mailers.smtp.host'), config('mail.mailers.smtp.port'), config('mail.mailers.smtp.encryption')))
            ->setUsername(config('mail.mailers.smtp.username'))
            ->setPassword(config('mail.mailers.smtp.password'));

        $mailer = new Swift_Mailer($transport);

        $message = (new Swift_Message($subject))
            ->setFrom($from, $sender)
            ->setTo($to)
            ->setContentType("text/html")
            ->setBody($body);

        $mailer->send($message);

        if ($mailer) {
            //            add to email
            $insertToEmail = Email::insertIntoEmail($userId, $accountId, 0, $subject, "", "");
            $emailId = $insertToEmail;

            //            add to send email
            $insertToSend = Email::insertIntoEmailSend($emailId, $to, $receiverName, $date);

            if ($insertToEmail && $insertToSend) $success = true;
        }

        return response([
            'success' => $success
        ]);
    }

    public function removeAccounts(Request $request)
    {
        $remove = false;

        $id = $request->id;

        $email = Email::getAccountEmail($id);

        //        delete emails
        $deleteEmail = Email::deleteEmailByAccount($id);
        //        delete account
        $deleteAccount = Email::deleteAccount($id);
        //        delete email_config
        if ($email) Email::deleteEmailConfig($email);

        if ($deleteEmail && $deleteAccount) $remove = true;

        return response([
            'remove' => $remove
        ]);
    }
}
