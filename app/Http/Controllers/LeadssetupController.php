<?php

namespace App\Http\Controllers;

use App\Models\Leadssetup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LeadssetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $setup_order_data =Leadssetup::select('*')->where('user_id', '=', auth()->user()->id)->get();
        return view('lead.leadssetup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $last = Leadssetup::create($request->all());
        return redirect()->route('lead.leadssetup')
            ->with('status', 'New Lead Step created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Leadssetup  $leadssetup
     * @return \Illuminate\Http\Response
     */
    public function show(Leadssetup $leadssetup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Leadssetup  $leadssetup
     * @return \Illuminate\Http\Response
     */
    public function edit(Leadssetup $leadssetup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Leadssetup  $leadssetup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Leadssetup $leadssetup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Leadssetup  $leadssetup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Leadssetup $leadssetup)
    {
        //
    }
}
