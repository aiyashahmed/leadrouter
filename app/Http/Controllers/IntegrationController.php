<?php

namespace App\Http\Controllers;

use App\Models\Integration_setup;
use App\Models\GmailCredential;
use Illuminate\Http\Request;
use Auth;

class IntegrationController extends Controller
{

    public function integrate_voip(Request $request, $name)
    {
        $userId = Auth::user()->id;

        $data = Integration_setup::all();

        $is_google_connected = GmailCredential::is_connected($userId);

        if($name == "email") return view('integration.integrate_email', compact('data','name', 'is_google_connected'));

        return view('integration.integrate_voip', compact('data','name'));
    }

    public function integrade_voip_install()
    {

        $data = Integration_setup::all();
        return view('integration.integrade_voip_install', compact('data'));
    }


    public function ivoip_save(Request $request)
    {
        Integration_setup::create($request->all());
        return redirect()->route('integrate')
            ->with('status', 'Voip Installed successfully.');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Integration_setup::all();
        return view('integration.integrate_setup', compact('data'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Integration_setup  $integration_setup
     * @return \Illuminate\Http\Response
     */
    public function show(Integration_setup $integration_setup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Integration_setup  $integration_setup
     * @return \Illuminate\Http\Response
     */
    public function edit(Integration_setup $integration_setup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Integration_setup  $integration_setup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Integration_setup $integration_setup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Integration_setup  $integration_setup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Integration_setup $integration_setup)
    {
        //
    }
}
