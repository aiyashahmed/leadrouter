<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Models\User;

use App\Providers\RouteServiceProvider;

class CompanyController extends Controller
{
    public function active(Request $data)
    {
        user::where('id', '=', $data->user_id)
            ->update(['act_id' => $data->com_id]);
            return redirect()->route('home');
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'subdomain' => ['required', 'string', 'max:255', 'unique:companies'],
            'user_id' => 'required'
        ]);

        $last = Company::create($request->all());
        user::where('id', '=', $request->user_id)
            ->update(['act_id' => $last->id]);
        return redirect()->route('company_create')
            ->with('status', 'Company created successfully.');

    }

}
