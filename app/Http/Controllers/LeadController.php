<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\leadspipeline;
use Illuminate\Http\Request;
use Auth;
use App\Imports\LeadImport;
use App\Exports\LeadExport;
use App\Exports\ContactExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class LeadController extends Controller
{
    public function view()
    {
        $user_id = Auth::user()->id;
        $result = leadspipeline::where('user_id', '=', $user_id)->get()->all();

        if (!$result) {
            $data = [
                ['heading' => 'INCOMING LEADS', 'step_no' => 1, 'color' => '#6c757d', 'user_id' => $user_id],
                ['heading' => 'INITIAL CONTACTS', 'step_no' => 2, 'color' => '#A3E4D7', 'user_id' => $user_id],
                ['heading' => 'CONTRACT DISCUSSION', 'step_no' => 3, 'color' => '#F9E79F', 'user_id' => $user_id],
                ['heading' => 'DECISION MAKING', 'step_no' => 4, 'color' => '#A9CCE3', 'user_id' => $user_id]
            ];
            leadspipeline::insert($data);
        }

        $pipeline = leadspipeline::select('*')
            ->where('user_id', '=', $user_id)
            ->orderBy('step_no', 'asc')->get();

        $leadCount = DB::table("leads")->join('leadspipeline','leadspipeline.id','leads.lead_step')
            ->where("leadspipeline.user_id",$user_id)->get()->count();

        return view('lead.leads_view', compact('pipeline','leadCount'));
    }

    public function create(Request $request)
    {

        // print_r($request->all());die;
        $user_id = Auth::user()->id;
        $pipeline = leadspipeline::select('*')
            ->where('user_id', '=', $user_id)
            ->where('heading','!=','INCOMING LEADS')
            ->orderBy('step_no', 'asc')->get();

        $lead =  Lead::select('*')
            ->where('id', '=', $request->id)->first();

        return view('lead.lead_create', compact('pipeline', 'lead'));
    }

    public function edit(Request $request)
    {
        $user_id = Auth::user()->id;
        $pipeline = leadspipeline::select('*')
            ->where('user_id', '=', $user_id)
            ->where('heading','!=','INCOMING LEADS')
            ->orderBy('step_no', 'asc')->get();

        $lead =  Lead::select('*')
            ->where('id', '=', $request->id)->get()->first();
        return view('lead.lead_edit', compact('pipeline', 'lead'));
    }

    public function lead_setup()
    {
        return view('lead.lead_setup');
    }

    public function new_lead_step()
    {
        return view('lead.new_lead_step');
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'p_contact' => 'required'
        ]);

        Lead::addLead($request->all());
        return redirect()->route('lead_create')
            ->with('status', 'Lead created successfully.');
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'p_contact' => 'required'
        ]);

        Lead::where('id', '=', $request->lead_id)
            ->update([
                'pipeline_id'=> $request->pipeline_id,
                'subject'=> $request->subject,
                'lead_step'=> $request->lead_step,
                'name'=> $request->name,
                'email'=> $request->email,
                'p_contact'=> $request->p_contact,
                'position'=> $request->position,
                'company'=> $request->company,
                'c_contact'=> $request->c_contact,
                'source'=> $request->source,
                'address'=> $request->address
            ]);
        return redirect()->route('leads_view')
            ->with('status', 'Lead updated successfully.');
    }

    public function import(Request $request)
    {
        Excel::import(new LeadImport, $request->file);

        return back();
    }

    public function leadsImport(Request $request)
    {

        $success = false;
        $success = true;
        try {
        $file =$request->import_file;
         Excel::import(new LeadImport, $file);

        //  return response([
        //     'success' => $success,
        // ]);

        } catch (\Exception $e) {
            return back()->with('status', 'Lead imports function error. please correct your file');
        }

        return back()->with('status', 'Lead imported successfully.');
    }

    public function export()
    {
        return Excel::download(new LeadExport, 'leads.xlsx');
    }

    public function stepChange(Request $request){

        $step_no = $request->step_no;
        $lead_id = $request->lead_id;
        $order_no = $request->new_lead_place;

        Lead::stepChange($lead_id,$step_no,$order_no);
    }

    public function contact_export(){
        return Excel::download(new ContactExport, 'Contacts.xlsx');
    }
}
