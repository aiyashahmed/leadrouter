<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\leadspipeline;
use Illuminate\Http\Request;
use Auth;
use DB;

class LeadspipelineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $pipeline = leadspipeline::select('*')
            ->where('user_id', '=', $user_id)
            ->orderBy('step_no', 'asc')->get()->all();

        return view('lead.leadspipeline', compact('pipeline'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        print_r($request->all());die;
        $user_id = Auth::user()->id;

        $steps = leadspipeline::select('id', 'heading', 'step_no')
            ->where('step_no', '>=', $request->no)
            ->where('user_id', '=', $user_id)
            ->orderBy('step_no', 'asc')->get()->all();

        $no = $request->no;

        foreach ($steps as $data) {
            leadspipeline::where('id', '=', $data->id)
                ->update(['step_no' => $data->step_no + 1]);
//            $no++;
        }

        $last_id = DB::table("leadspipeline")->insertGetId([
            'heading' => $request->heading,
            'step_no' => $request->no,
            'color' => $request->color,
            'user_id' => $user_id
        ]);

//        die;
//
//        leadspipeline::where('id', '=', $last_id)
//            ->where('user_id', '=', $user_id)
//            ->update(['step_no' => $request->no]);
//
//        die;
//
//        return redirect()->route('leadspipeline')
//            ->with('status', 'new step created successfully.');

        if($last_id) return response([
            'error' => 0
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\leadspipeline  $leadspipeline
     * @return \Illuminate\Http\Response
     */
    public function show(leadspipeline $leadspipeline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\leadspipeline  $leadspipeline
     * @return \Illuminate\Http\Response
     */
    public function edit(leadspipeline $leadspipeline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\leadspipeline  $leadspipeline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, leadspipeline $leadspipeline)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\leadspipeline  $leadspipeline
     * @return \Illuminate\Http\Response
     */
    public function destroy(leadspipeline $leadspipeline)
    {
        //
    }

    public function updateName(Request $request){
        $id = $request->id;
        $name = $request->name;

        Lead::updateLeadspipelineName($id,$name);
    }

    public function deletePipeline(Request $request){
        $id = $request->id;
        $user_id = Auth::user()->id;

        Lead::deletePipeline($user_id,$id);

        return response([
            'error' => 0
        ]);
    }
}
