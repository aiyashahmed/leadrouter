<?php

namespace App\Imports;

use Auth;
use App\Models\Lead;
use App\Models\leadspipeline;
use Maatwebsite\Excel\Concerns\ToModel;

use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LeadImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $user_id = Auth::user()->id;
        $pipeline = leadspipeline::select('id')
            ->where('user_id', '=', $user_id)
            ->orderBy('step_no', 'asc')->first();

        return new Lead([
            'lead_step' => $pipeline->id,
            'subject' => $row['subject'],
            'name' => $row['name'],
            'email' => $row['email'],
            'p_contact' => $row['p_contact'],
            'position' => $row['position'],
            'company' => $row['company'],
            'c_contact' => $row['c_contact'],
            'address' => $row['address'],
            'user_id' => $user_id
        ]);
    }
}
