<?php

namespace App\Exports;

use DB;
use App\Models\Lead;
use App\Models\leadspipeline;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Support\Arr;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class ContactExport implements FromView
{
    public function view(): View
    {
        return view('pages.contact_view_export');
    }

    
}
