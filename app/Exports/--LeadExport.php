<?php

namespace App\Exports;

use App\Models\Lead;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;

class LeadExport implements FromCollection, WithHeadings, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $user_id = Auth::user()->id;
        $leadData = Lead::select('subject','name','email','p_contact','c_contact','address','created_at')
            ->where('user_id', '=', $user_id)
            ->orderBy('created_at', 'asc')->get();
        return $leadData;
    }

    public function headings(): array
    {
        return [
            'SUBJECT','NAME','EMAIL','PERSONAL CONTACT','COMPANY CONTACT','ADDRESS','CREATED TIME'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }

}
