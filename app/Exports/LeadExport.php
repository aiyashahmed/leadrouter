<?php

namespace App\Exports;

use DB;
use App\Models\Lead;
use App\Models\leadspipeline;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Support\Arr;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class LeadExport implements FromView, WithHeadings
{
    public function view(): View
    {
        return view('lead.export');
    }

    public function headings(): array
    {
        $user_id = Auth::user()->id;
        $pipeline = leadspipeline::select(DB::raw('CONCAT(id,"-",heading) as heading '))
            ->where('user_id', '=', $user_id)
            ->orderBy('step_no', 'asc')->get();
        $pipeline = $pipeline->toArray();
        return   Arr::flatten($pipeline);
    }
}
