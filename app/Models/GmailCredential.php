<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Email;

class GmailCredential extends Model
{
    use HasFactory;

    protected $table = "user_gmail_credentials";


    public static function newCredentials($data, $user_id)
    {
        $remove = DB::table('user_gmail_credentials')->where('user_id', $user_id)->where('google_user_id', $data->id)->delete();

        $add = DB::table('user_gmail_credentials')->insert([
            'user_id' => $user_id,
            'access_token' => $data->token,
            'refresh_token' => $data->refreshToken,
            'expires_in' => $data->expiresIn,
            'google_user_id' => $data->id,
            'name' => $data->name,
            'avatar' => $data->avatar,
            'email' => $data->email
        ]);

        $existAccount = Email::existAccount($data->email);

        if(!$existAccount) {
            $add = DB::table("email_accounts")->insert([
                'user_id' => $user_id,
                'email' => $data->email
            ]);
        }

        if ($add) return true;
    }

    public static function is_connected($userId)
    {
        $check = DB::table('user_gmail_credentials')->where('user_id', $userId)->first();
        if ($check) return true;
    }

    public static function getUserData($userId)
    {
        $data = DB::table('email_accounts')->where('user_id', $userId)->get();
        return $data;
    }
}
