<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class leadspipeline extends Model
{
    use HasFactory;

    protected $table = 'leadspipeline';
    public $timestamps = true;

    protected $fillable = [
        'heading',
        'step_no',
        'user_id',
        'color'
    ];
}
