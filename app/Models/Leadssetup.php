<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Leadssetup extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'Leadssetup';
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'heading',
        'step_no',
        'color',
        'status',
        'user_id'
    ];
}
