<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Email extends Model
{
    use HasFactory;

    public static function verifyAccount($host, $email, $password)
    {
        $email = DB::table('email_config')->where('host', $host)->where('username', $email)->where('password', $password)->first();
        if (!empty($email)) return true;
    }

    public static function insertIntoEmailConfig($user_id, $host, $username, $password)
    {
        $insert = DB::table('email_config')->insertGetId([
            'user_id' => $user_id,
            'host' => $host,
            'username' => $username,
            'password' => $password
        ]);

        $existAccount = self::existAccount($username);

        if (!$existAccount) {
            $insert = DB::table('email_accounts')->insert([
                'user_id' => $user_id,
                'email' => $username
            ]);
        }

        if ($insert) return $insert;
    }

    public static function insertIntoEmail($user_id, $account_id = "", $msg_no, $subject = "", $body, $attachment = "")
    {
//        echo $msg_no."<br>";
//        echo $subject."<br>";

        $insert = DB::table('email')->insertGetId([
            'user_id' => $user_id,
            'msg_no' => $msg_no,
            'subject' => $subject,
            'body' => $body,
            'account_id' => $account_id
        ]);

        if ($insert) return $insert;
    }

    public static function insertIntoEmailReceive($email_id, $from, $name = "", $date)
    {
        $insert = DB::table('email_receive')->insert([
            'email_id' => $email_id,
            'from' => $from,
            'name' => $name,
            'date' => $date
        ]);

        if ($insert) return true;
    }

    public static function insertIntoEmailSend($email_id, $to, $name = "", $date)
    {
        $insert = DB::table('email_send')->insert([
            'email_id' => $email_id,
            'to' => $to,
            'name' => $name,
            'date' => $date
        ]);

        if ($insert) return true;
    }

    public static function insertIntoEmailTrash($email_id, $from, $name = "", $date)
    {
        $insert = DB::table('email_trash')->insert([
            'email_id' => $email_id,
            'from' => $from,
            'name' => $name,
            'date' => $date
        ]);

        if ($insert) return true;
    }

    public static function emailExist($msg_no, $user_id)
    {
        $check = DB::table('email')->where('msg_no', $msg_no)->where('user_id', $user_id)->first();
        if (!empty($check)) return true;
    }

    public static function getInbox($user_id)
    {
        $email = DB::table('email')
            ->join('email_receive', 'email_receive.email_id', 'email.id')
            ->join('email_accounts', 'email_accounts.id', 'email.account_id')
            ->where('email.user_id', $user_id)
            ->select('email.subject', 'email.msg_no', 'email_receive.name as from', 'email_receive.date as received_date', 'email.account_id', 'email_accounts.email as account')->get();
        return $email;
    }

    public static function sendbox($user_id)
    {
        $email = DB::table('email')
            ->join('email_send', 'email_send.email_id', 'email.id')
            ->join('email_accounts', 'email_accounts.id', 'email.account_id')
            ->where('email.user_id', $user_id)
            ->select('email.subject', 'email.msg_no', 'email_send.name as to', 'email_send.date as sent_date', 'email.account_id', 'email_accounts.email as account')->get();
        return $email;
    }

    public static function getTrash($user_id)
    {
        $email = DB::table('email')
            ->join('email_trash', 'email_trash.email_id', 'email.id')
            ->join('email_accounts', 'email_accounts.id', 'email.account_id')
            ->where('email.user_id', $user_id)
            ->select('email.subject', 'email.msg_no', 'email_trash.name', 'email_trash.date as date', 'email.account_id', 'email_accounts.email as account')->get();
        return $email;
    }

    public static function getAccountId($email)
    {
        $data = DB::table('email_accounts')->where('email', $email)->first();

        return $data->id;
    }

    public static function getAccountDataByEmail($email)
    {
        $data = DB::table('email_accounts')->where('email', $email)->first();

        return $data;
    }

    public static function getAccountEmail($id){
        $data = DB::table('email_accounts')->where('id', $id)->first();

        if($data) return $data->email;
    }

    public static function existAccount($email)
    {
        $exist = DB::table('email_accounts')->where('email', $email)->first();

        if (!empty($exist)) return true;
    }

    public static function deleteEmailByAccount($id){
        $data = DB::table('email')->where('account_id',$id)->get();
        foreach($data as $d){
            self::deleteEmail($d->id);
        }

        return true;
    }

    public static function deleteEmail($id){
        DB::table('email')->where('id',$id)->delete();
        DB::table('email_receive')->where('email_id',$id)->delete();
        DB::table('email_send')->where('email_id',$id)->delete();
        DB::table('email_trash')->where('email_id',$id)->delete();

        return true;
    }

    public static function deleteAccount($id){
        $delete = DB::table('email_accounts')->where('id',$id)->delete();

        if($delete) return true;
    }

    public static function deleteEmailConfig($email){
        $delete = DB::table('email_config')->where('username',$email)->delete();

        if ($delete) return true;
    }
}
