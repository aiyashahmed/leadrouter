<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Integration_setup extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'integration_setup';
    public $timestamps = true;

    protected $fillable = [
        'name',
        'user_name',
        'password',
        'script_path',
        'port',
        'int_status',
        'user_id',
        'com_id',
        'status'
    ];
}
