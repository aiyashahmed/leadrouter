<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;
use phpDocumentor\Reflection\Types\This;

class Lead extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pipeline_id',
        'lead_step',
        'subject',
        'name',
        'email',
        'p_contact',
        'position',
        'company',
        'c_contact',
        'source',
        'address',
        'country',
        'user_id',
        'com_id',
        'status'
    ];

    public static function addEmailLeads($subject, $email, $name, $user_id, $messageDate)
    {
        $lead_step = (new Lead)->getIncomingLeadId($user_id);

        $order = (new lead)->getLastOrder($lead_step);

        $add = DB::table('leads')->insert([
            'pipeline_id' => 1,
            'subject' => $subject,
            'lead_step' => $lead_step,
            'email' => $email,
            'name' => $name,
            'user_id' => $user_id,
            'order' => $order + 1,
            'created_at' => date('H:i d/m/Y', strtotime($messageDate))
        ]);

        if ($add) return true;
    }

    public static function addLead($data)
    {
        $order = (new lead)->getLastOrder($data['lead_step']);

        $add = DB::table('leads')->insert([
            'pipeline_id' => $data['pipeline_id'],
            'subject' => $data['subject'],
            'lead_step' => $data['lead_step'],
            'email' => $data['email'],
            'name' => $data['name'],
            'user_id' => $data['user_id'],
            'country' => $data['country'],
            'source' => $data['source'],
            'p_contact' => $data['p_contact'],
            'position' => $data['position'],
            'company' => $data['company'],
            'c_contact' => $data['c_contact'],
            'address' => $data['address'],
            'order' => $order + 1,
            'created_at' => \Carbon\Carbon::now()
        ]);

        if ($add) return true;
    }

    private function getIncomingLeadId($user_id)
    {
        $data = DB::table('leadspipeline')->where('user_id', $user_id)->orderBy('id', 'asc')->first();
        return $data->id;
    }

    private function getLastOrder($lead_step)
    {
        $leads = DB::table('leads')->where('lead_step', $lead_step)->orderBy('id', 'desc')->first();

        if (empty($leads)) return 0;

        return $leads->order;
    }

    public static function stepChange($lead_id, $step_no, $order_no)
    {

        if ($order_no == 0) $order_no = 1;

//        change order number from previous
        (new Lead)->changePreviousOrder($lead_id);

//        change order number current place
        (new Lead)->changeCurrentOrder($step_no, $order_no);

//        update current changes
        DB::table('leads')->where('id', $lead_id)->update([
            'lead_step' => $step_no,
            'order' => $order_no
        ]);
    }

    private function changePreviousOrder($lead_id)
    {
        $lead = DB::table('leads')->find($lead_id);

        $lead_step = $lead->lead_step;
        $prev_order = $lead->order;

        $leads = DB::table('leads')->where('lead_step', $lead_step)->where('order', '>', $prev_order)->get();

        foreach ($leads as $lead) {
            $id = $lead->id;
            $order = $lead->order;

            if (!$order == 0) {
                DB::table('leads')->where('id', $id)->update([
                    'order' => ($order - 1)
                ]);
            }

        }
    }

    private function changeCurrentOrder($step_no, $order)
    {
        $leads = DB::table('leads')->where('lead_step', $step_no)->where('order', '>=', $order)->get();

        foreach ($leads as $lead) {
            $id = $lead->id;
            $order = $lead->order;

            if (!$order == 0) {
                DB::table('leads')->where('id', $id)->update([
                    'order' => ($order + 1)
                ]);
            }
        }
    }

    public static function updateLeadspipelineName($id,$heading){
        echo $id;
        echo $heading;
        DB::table("leadspipeline")->where("id",$id)->update([
            'heading' => $heading
        ]);
    }

    public static function deletePipeline($user_id,$id){

//        reorder step_no
        $total = DB::table("leadspipeline")->where("user_id",$user_id)->count();

        DB::table("leadspipeline")->where("id",$id)->update([
            'step_no' => $total
        ]);


        $data = DB::table("leadspipeline")->where("user_id",$user_id)->orderby("step_no","asc")->get();
        $count = 1;
        foreach($data as $d) {

            $id = $d->id;
            DB::table("leadspipeline")->where("id",$id)->update([
                "step_no" => $count++
            ]);
        }

        DB::table("leadspipeline")->where("user_id",$user_id)->orderby('step_no','desc')->limit(1)->delete();
//        make it last
//        delete
    }
}
