<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNameAvatarEmailColumnsToUserGmailCredentials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_gmail_credentials', function (Blueprint $table) {
            $table->string('name');
            $table->longText('avatar');
            $table->string('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_gmail_credentials', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('avatar');
            $table->dropColumn('email');
        });
    }
}
