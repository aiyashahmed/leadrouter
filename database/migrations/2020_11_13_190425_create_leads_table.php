<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->integer('pipeline_id')->nullable();
            $table->integer('lead_status')->default(1);
            $table->string('name');
            $table->string('email')->nullable();
            $table->integer('p_contact')->nullable();
            $table->date('position')->nullable();
            $table->string('company')->nullable();
            $table->integer('c_contact')->nullable();
            $table->string('source')->nullable();
            $table->string('Address')->nullable();
            $table->string('country')->nullable();
            $table->integer('user_id');
            $table->integer('com_id')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
