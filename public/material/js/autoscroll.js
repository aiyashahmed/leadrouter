/*
See https://greensock.com/gsap-1-16 for details.
This demo uses ThrowPropsPlugin which is a membership benefit of Club GreenSock, https://greensock.com/club/
*/
var $snap = $("#snap"),
    $liveSnap = $("#liveSnap"),
    $container = $("#container"),
    gridWidth = 74,
    gridHeight = 50,
    gridRows = 28,
    gridColumns = 40,
    i, x, y;

//loop through and create the grid (a div for each cell). Feel free to tweak the variables above
for (i = 0; i < gridRows * gridColumns; i++) {
    y = Math.floor(i / gridColumns) * gridHeight;
    x = (i * gridWidth) % (gridColumns * gridWidth);
    $("<div/>").css({position:"absolute", border:"1px solid #454545", width:gridWidth-1, height:gridHeight-1, top:y, left:x}).prependTo($container);
}

//set the container's size to match the grid, and ensure that the box widths/heights reflect the variables above
TweenLite.set($container, {height: gridRows * gridHeight + 1, width: gridColumns * gridWidth + 1});
TweenLite.set(".box", {width:150, height:100, lineHeight:"100px"});

//the update() function is what creates the Draggable according to the options selected (snapping).
function update() {
    var snap = $snap.prop("checked"),
        liveSnap = $liveSnap.prop("checked");
    Draggable.create(".box", {
        bounds:$container,
        autoScroll:1,
        edgeResistance:0.65,
        type:"x,y",
        throwProps:true,
        liveSnap:liveSnap,

        //if the element gets thrown outside the wrapper, scroll accordingly to center it.
        onRelease: function() {
            this.tween.progress(1); //put the element at the END of where it's thrown so we can calculate bounds accordingly
            var wrapper = document.querySelector(".wrapper"),
                tBounds = this.target.getBoundingClientRect(),
                wBounds = wrapper.getBoundingClientRect(),
                wCenter = wBounds.left + (wBounds.width / 2),
                tCenter = tBounds.left + (tBounds.width / 2),
                scroll = {};
            if (tBounds.right > wBounds.right || tBounds.left < wBounds.left) {
                scroll.x = wrapper.scrollLeft + (tCenter - wCenter);
            }
            if (tBounds.bottom > wBounds.bottom || tBounds.top < wBounds.top) {
                wCenter = wBounds.top + (wBounds.height / 2);
                tCenter = tBounds.top + (tBounds.height / 2);
                scroll.y = wrapper.scrollTop + (tCenter - wCenter);
            }
            TweenMax.to(wrapper, this.tween.duration(), {scrollTo:scroll});
            this.tween.progress(0); //now return the element to where it was thrown from.
        },

        snap:{
            x: function(endValue) {
                return (snap || liveSnap) ? Math.round(endValue / gridWidth) * gridWidth : endValue;
            },
            y: function(endValue) {
                return (snap || liveSnap) ? Math.round(endValue / gridHeight) * gridHeight : endValue;
            }
        }
    });
}

//when the user toggles one of the "snap" modes, make the necessary updates...
$snap.on("change", applySnap);
$liveSnap.on("change", applySnap);

function applySnap() {
    if ($snap.prop("checked") || $liveSnap.prop("checked")) {
        $(".box").each(function(index, element) {
            TweenLite.to(element, 0.5, {
                x:Math.round(element._gsTransform.x / gridWidth) * gridWidth,
                y:Math.round(element._gsTransform.y / gridHeight) * gridHeight,
                delay:0.1,
                ease:Power2.easeInOut
            });
        });
    }
    update();
}

update();
