$("#menu .hoverable-li").hover(function () {
    var ul = $(this);

    $(".sidebar").css({"width": "245px", "background": "#eff2f5"});
    $(".sidebar").css({"z-index": 4});
    ul.find("ul").css({"visibility": "visible", "opacity": 1, "position": "fixed", "display": "block"
    ,"top":"24px","left":"69px"});
    // setTimeout(function () {
    // },100);
    $("#menu .hoverable-li").find('ul').hide();
    ul.find('ul').show();

    $(".sidebar-wrapper").css({"width": "245px"});
    $(".navbar").css({"z-index": 1});
}, function () {
    // $(".sidebar").css({"width": "70px", "background": "#0a1b2b"});
    // $(this).find("ul").css({"visibility": "hidden", "opacity": 0, "display": "none"});
    // $(".sidebar-wrapper").css({"width": "70px"});
})

// mouse out
$(".sidebar").on({
    "mouseleave" : function(){
        $(this).css({"width": "70px", "background": "#0a1b2b"});
        $("#menu .hoverable-li").find("ul").css({"visibility": "hidden", "opacity": 0, "display": "none"});
        $(".sidebar-wrapper").css({"width": "70px"});
    }
})

$(".hoverable-ul").on({
    "mouseleave" : function(){
        console.log("leave");
        $(".sidebar").css({"width": "70px", "background": "#0a1b2b"});
        $("#menu .hoverable-li").find("ul").css({"visibility": "hidden", "opacity": 0, "display": "none"});
        $(".sidebar-wrapper").css({"width": "70px"});
    }
})

$("#menu .hoverable-li ul > li").hover(function(){
    $(this).css({"background": "#f9f6f254"});
},function(){
    $(this).css({"background": "#f9f6f254"});
})
