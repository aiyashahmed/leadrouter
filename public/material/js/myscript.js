$(function () {

    //    text editor
    $("#txtEditor").Editor({
        fonts: false,
        color: false,
        styles: false,
        // font_size: false,
        undo: false,
        redo: false,
        // ol: false,
        // ul: false,
        unlink: false,
        insert_link: false,
        insert_img: false,
        insert_table: false,
        hr_line: false,
        block_quote: false,
        indent: false,
        outdent: false,
        print: false,
        rm_format: false,
        select_all: false,
        togglescreen: false,
        splchars: false,
        source: false,
        strikeout: false
    });

    //    email inbox datatable
    $inboxEmailTable = $('#email-inbox-table').DataTable({
        ajax: $base_url + "/email/get/inbox",
        searching: true,
        columnDefs: [{
            targets: [3],
            visible: false
        }],
        columns: [
            {data: "from"},
            {data: "subject"},
            {data: "received_date"},
            {data: "account"},
        ]
    });

    //    email sendbox datatable
    $sendEmailTable = $('#email-send-box-table').DataTable({
        ajax: $base_url + "/email/get/sendbox",
        searching: true,
        columnDefs: [{
            targets: [3],
            visible: false
        }],
        columns: [
            {data: "to"},
            {data: "subject"},
            {data: "sent_date"},
            {data: "account"}
        ]
    });

    // email trash datatable
    $trashEmailTable = $('#email-trash-box-table').DataTable({
        ajax: $base_url + "/email/get/trash",
        searching: true,
        columnDefs: [{
            targets: [3],
            visible: false
        }],
        columns: [
            {data: "name"},
            {data: "subject"},
            {data: "date"},
            {data: "account"},
        ]
    });

    // hide default searchbox in datatables
    $("#email-inbox-table_filter").hide();
    $("#email-send-box-table_filter").hide();
    $("#email-trash-box-table_filter").hide();

    //    default account selected in mail datatables
    selectAccount();

    //    connect emails
    $("#get-all-gmail-btn").click(function () {
        var btn = $(this);
        var url = $base_url + "/email/gmail/extract";
        var type = "get";
        var data = "";
        var errorDiv = $("#email-connect-error");
        errorDiv.text("");
        btn.text("Loading..");

        ajPost(url, type, data, function (r) {
            if (r.connect == 1) {
                btn.text("Click!")
                errorDiv.addClass('text-success').removeClass('text-danger').text("Succeed! check your emails.");
            } else if (r.connect == 2) {
                errorDiv.removeClass('text-success').addClass('text-danger').html("Please login again with google </br> by clicking the another account button!");
                btn.text("Click!");
            } else {
                btn.text("Try Again!");
            }
        }, function (status) {
            if (status == 500) alert("something went wrong!");
        }, false)
    })

    //    hideshow email setups
    $("#gmail-btn").click(function () {
        hideShow('.setup-div', 'hide');
        hideShow('#gmail-div', 'show');
    })

    $("#yahoo-btn").click(function () {
        hideShow('.setup-div', 'hide');
        hideShow('#yahoo-div', 'show');
    })

    $("#imap-btn").click(function () {
        hideShow('.setup-div', 'hide');
        hideShow('#imap-div', 'show');
    })

    //    get emails by email
    $("#email-account-select").change(function () {
        var email = $(this).val();
        $inboxEmailTable.columns(3).search(email).draw();
        $sendEmailTable.columns(3).search(email).draw();
        $trashEmailTable.columns(3).search(email).draw();
    })

    //    smtp/imap save to db
    $("#email-setting-form").submit(function (e) {
        e.preventDefault();
        var btn = $("#email-setting-form #connect-btn");
        var error = false;
        var url;
        var data;
        var type;
        var errorDiv = $("#email-setting-error");
        var progress = $(".progress");
        var progressBar = $(".progress-bar");

        errorDiv.text("");
        progress.addClass('d-none');

        $.each($("#email-setting-form input"), function (i) {
            var name = $(this).attr('name');
            var value = $(this).val();

            if (value == "") {
                error = true;
                errorDiv.fadeIn().text("All the fields must be entered.");
            }
        });

        setTimeout(function () {
            errorDiv.fadeOut();
        }, 3000)

        if (!error) {
            btn.text("Loading..");

            progress.removeClass('d-none');
            $(".progress-bar").css("background", "#fab95a");
            $(".progress-bar").width(2 + "%");

            url = $base_url + "/email/hotmail";
            data = $(this).serialize();
            type = "post";


            ajPost(url, type, data, function (r) {
                if (r.success == false) {
                    progressBar.css("background", "#e62f2f");
                    $(".progress-bar").width(100 + "%");
                    progressBar.html("Failed!");
                    errorDiv.fadeIn().text("The entered credentials are wrong!");
                } else {
                    $(".progress-bar").width(100 + "%");
                    $(".progress-bar").html(100 + "%");
                    $("#email-account-select").prepend("<option id='opt-" + r.id + "' value='" + r.email + "'>" + r.email + "</option>");

                    $("#remove-accounts-table").prepend("<tr id='tr-" + r.id + "'>" +
                        "<td>" + r.email + "</td>" +
                        "<td>" +
                        "<button class='btn btn-sm btn-outline-warning account-remove-btn' id='" + r.id + "'>" +
                        "<i class='fa fa-window-close text-danger'></i>" +
                        "</button>" +
                        "</td>" +
                        "</tr>");

                    emailTableReload();
                    selectAccount();

                    progress.addClass('d-none');
                }
                btn.text("connect");
            }, function (status) {
                if (status == 500) {
                    progressBar.html("Failed!");
                    errorDiv.fadeIn().text("The entered credentials are wrong!");
                }
            });
        }
    })

    //    custom table searching
    $("#table-search").submit(function (e) {
        e.preventDefault();
        var val = $("#table-search input[name='table-search']").val();

        $inboxEmailTable.search(val).draw();
        $sendEmailTable.search(val).draw();
        $trashEmailTable.search(val).draw();

        var inbox = $inboxEmailTable.rows({search: 'applied'}).count();
        var sent = $sendEmailTable.rows({search: 'applied'}).count();
        var trash = $trashEmailTable.rows({search: 'applied'}).count();

        $("#email-total-count").text(inbox + sent + trash);

        if (val == "") $("#email-total-count").text(0);
    })

    //    select email
    $("#select-email").change(function () {
        var val = $(this).val();

        if (val == "inbox") {
            hideShow("#inbox-div", "show");
            hideShow("#sendbox-div", "hide");
            hideShow("#trashmail-div", "hide");
        } else if (val == "sent") {
            hideShow("#inbox-div", "hide");
            hideShow("#sendbox-div", "show");
            hideShow("#trashmail-div", "hide");
        } else {
            hideShow("#inbox-div", "hide");
            hideShow("#sendbox-div", "hide");
            hideShow("#trashmail-div", "show");
        }
    })

    $("#compose-page-btn").click(function () {
        if ($("#email-account-select").has('option').length == 0) {
            Swal.fire(
                'Error!',
                'No accounts have been connected.',
                'warning'
            );
        } else {
            window.location.replace($base_url + '/email/compose');
        }
    })

    //    send email
    $("#mail-send").click(function () {
        var error = false;
        var btn = $(this);
        var errorDiv = $("#error-div");
        var url;
        var data;
        var type;

        errorDiv.text("");

        $.each($("#compose-mail-form input"), function (i) {
            if ($(this).val() == "") error = true;
        })

        if ($("#compose-mail-form .Editor-editor").text() == "") error = true;

        if (error) {
            errorDiv.text("Please fill the all the fields!");
        }

        if (!validateEmail($("#compose-mail-form input[name='to']").val())) {
            error = true;
            errorDiv.text("Please enter the valid email address");
        }

        if (!error) {
            btn.text("Sending..");
            url = $base_url + "/email/send/mail";
            data = {
                'from': $("#email-account-select").val(),
                'to': $("#compose-mail-form input[name='to']").val(),
                'subject': $("#compose-mail-form input[name='subject']").val(),
                'message': $("#compose-mail-form .Editor-editor").html()
            };
            type = "post";
            ajPost(url, type, data, function (r) {
                if (r.success) {
                    Swal.fire(
                        'Sent!',
                        'The email sent successfully.',
                        'success'
                    );
                    $("#compose-mail-form").trigger('reset');
                    $("#compose-mail-form .Editor-editor").html("");
                }
            }, function (status) {
                if (!status == 200) errorDiv.text("Something went wrong try again.");
                btn.html("<div class='row'>\n" +
                    "                                    <i class='fa fa-telegram pr-1'></i> Send\n" +
                    "                                </div>");
            }, false)
        }
    })

    //    account remove
    $("#remove-accounts-table").on('click', '.account-remove-btn', function () {
        var btn = $(this);
        var id = btn.attr('id');
        var url = $base_url + '/email/remove/accounts';
        var data = {
            id,
        }
        var type = "post";

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, remove it!'
        }).then((result) => {
            if (result.isConfirmed) {
                btn.text("Loading..");
                ajPost(url, type, data, function (r) {
                    if (r.remove) {
                        Swal.fire(
                            'Removed!',
                            'Your account has been removed.',
                            'success'
                        )

                        $("#tr-" + id).remove();
                        $("#opt-" + id).prop('selected', false);
                        $("#opt-" + id).remove();

                        emailTableReload();

                        selectAccount();
                    }
                }, function (status) {
                    if (!status == 200) {
                        Swal.fire(
                            'Error!',
                            'something went wrong.',
                            'warning'
                        )
                        btn.html('<i class="fa fa-window-close text-danger"></i>');
                    }
                }, false)
            }
        })
    })

    //    show imap/smtp setting form
    $("#smtp-imap-btn").click(function () {
        hideShow("#smtp-form-div", 'show');
    })
    $("#smtp-form-cancel-btn").click(function () {
        hideShow("#smtp-form-div", 'hide');
    })

//    scroll by mouse
    $("#leadsTable").noscroll();

//    leads table search
    $("#leadsearch").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        tableSearch("#leadsTable .draggable-li", value);
    });

//    lead setup
    $("#lead-setup-table").on("click", ".add-lead-setup", function () {
        const id = $(this).data("id");
        let step_no = parseInt($(this).data("stepno")) + parseInt(1);
        const td = $(this).closest("td");

        $("#lead-setup-table td.lead-td").removeClass("lead-td");

        td.after("<td class='pipe_line_" + step_no + " form-td lead-td'>" +
            "<div>" +
            "<button class='btn btn-default btn-sm bg-light text-danger close-new-setup btn-"+id+"' style='top: -10px;left: 189px' data-id='" + id + "' data-no='" + step_no + "'>" +
            "<i class='fa fa-trash'></i>" +
            "</button>" +
            "</div>" +
            "<div class='card' style='width:250px;height: 500px;vertical-align: top;margin-top: -10px;'>" +
            "<form class='form-horizontal add-setup-form'>" +
            "<div class='card-header bg-secondary'>" +
            "<div>" +
            "<div class='form-group' style='margin-top:-10px; padding-top:-10px'>" +
            "<div class='row'>" +
            "<div class='text-left'>" +
            "<input type='hidden' name='step_no' value='" + id + "'>" +
            "<input type='hidden' name='no' value='" + step_no + "'>" +
            "<input type='text' class='form-control' name='heading' placeholder='Title' style='font-size: 16px; color:#fff;  text-transform: uppercase;'>" +
            "</div>" +
            "<div class='text-right'>" +
            "<input type='text' class='form-control color' name='color'>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</form>" +
            "</div>" +
            "</td>");
            // "<td class='pipe_line_" + step_no + " add-btn'>" +
            // "<button type='button' class='btn btn-light btn-sm add-lead-setup lead-setup-" + step_no + " mt-5 bg-light text-gray'" +
            // "data-id='" + id + "'" +
            // "data-stepno='" + step_no + "'" +
            // "><i class='fa fa-plus'></i></button>" +
            // "</td>");

        $("#lead-setup-table td.lead-td").nextAll().addClass("next-lead-setup");

        // if($("#lead-setup-table td.form-td").length > 1) {
        //     step_no = parseInt(step_no) + parseInt($("#lead-setup-table td.form-td").length);
        //     $("#btn-" + id).data("data-no",step_no);
        // }

        // $("#lead-setup-table td.next-lead-setup").each(function(){
        //     let prev_step_no = $(this).find(".add-lead-setup").data("stepno");
        //     console.log(prev_step_no);
        //     $(this).find(".add-lead-setup").data("stepno",parseInt(prev_step_no) + parseInt(1));
        // })

        colorPicker();

        $(".pipe_line_" + step_no).find(".input[name='color']").trigger("click");

        $(this).hide();
    });

//    add setup new
    $("#lead-setup-table").on("focusout", ".form-td", function () {
        let heading = $(this).find(".add-setup-form input[name='heading']").val();
        let step_no = $(this).find(".add-setup-form input[name='step_no']").val();
        let no = $(this).find(".add-setup-form input[name='no']").val();
        let color = $(this).find(".add-setup-form input[name='color']").val();
        let url = $base_url + "/leadspipeline-add";
        let type = "post";
        let data = {
            heading,
            step_no,
            no,
            color
        }

        if (color == "" || heading == "") {
            return;
        }

        $(".loading-div").removeClass("d-none");

        $(this).find(".close-new-setup").remove();

        $(this).find(".card").html("<div class='card-header lead-title-div' style='background-color:" + color + "; height:42px'><h4 class='card-title text-white' style='font-size:14px; text-transform: uppercase;'>" + heading + "</h4></div>" +
            "<div class='card-body'><div class=\"table-responsive\">\n" +
            "                                                        <table class=\"table\">\n" +
            "                                                            <thead class=\" text-primary\">\n" +
            "                                                            <th>\n" +
            "                                                            </th>\n" +
            "                                                            </thead>\n" +
            "                                                            <tbody>\n" +
            "                                                            <tr>\n" +
            "                                                                <td>\n" +
            "\n" +
            "                                                                </td>\n" +
            "                                                            </tr>\n" +
            "                                                            </tbody>\n" +
            "                                                        </table>\n" +
            "                                                    </div></div>").css("margin-top", "30px");

        ajPost(url, type, data, function (r) {
            location.reload();
        }, function () {
        }, false);
    });


    // setup background color change
    $("#lead-setup-table").on("change", ".add-setup-form input[name='color']", function () {
        if ($(this).val() == "") {
            return;
        }

        let color = $(this).val();
        $(this).closest(".card-header").removeClass("bg-secondary").css("background-color", color);
        $(this).closest("form").find("input[name='heading']").focus();
    })

//    close opened new setup
    $("#lead-setup-table").on("click", ".close-new-setup", function () {
        const id = $(this).data('id');
        const no = $(this).data('no');
        // $(this).closest("td").remove();

        $(".pipe_line_" + no).remove();
        $(".lead-setup-" + (no > 1 ? parseInt(no) - parseInt(1) : 0)).show();
    });

//    lead rename
    $("#lead-setup-table").on("dblclick", ".lead-title-div", function () {
        let title = $(this).children("h4").text();
        const id = $(this).data("id");

        if (title.trim() == "INCOMING LEADS") {
            return;
        }

        $(this).html("<input type='text' class='form-control text-white text-uppercase rename-inp' data-title='" + title.trim() + "' data-id='" + id + "' name='title' value='" + title.trim() + "'>" +
            "<div class='cancel-rename-div-" + id + "' style='\n" +
            "    position: relative;\n" +
            "    top: -89px;\n" +
            "    left: 116px;\n'><button class='btn btn-default btn-sm bg-light text-danger cancel-rename' data-title='" + title.trim() + "'><i class='fa fa-window-close'></i></button>" +
            "<button class='btn btn-default bg-light btn-sm text-danger delete-lead-setup' data-id='" + id + "'><i class='fa fa-trash'></i></button>" +
            "</div>");
    })

//    rename entered
    $("#lead-setup-table").on("focusout", ".rename-inp", function (e) {

        let name = $(this).val().toUpperCase();
        const id = $(this).data("id");
        let url = $base_url + "/leadspipeline/title-change";
        let type = "post";
        let data = {
            name,
            id
        };

        if(name == "") return;

        $(this).closest(".lead-title-div").html("<h4 class='card-title text-white' style='font-size:14px; text-transform: uppercase;'>" + name + "</h4>");
        $(".cancel-rename-div-" + id).remove();
        ajPost(url, type, data, function () {
        }, function () {
        }, false);
    })

//    rename cancel
    $("#lead-setup-table").on("click", ".cancel-rename", function () {
        let title = $(this).data("title");
        $(this).closest(".lead-title-div").html("<h4 class='card-title text-white' style='font-size:14px; text-transform: uppercase;'>" + title + "</h4>");
    })

//    delete lead setup
    $("#lead-setup-table").on("click",".delete-lead-setup",function(){
        $(this).closest("td").next().remove();
        $(this).closest("td").remove();
        const id = $(this).data("id");
        let url = $base_url + "/leadspipeline-delete";
        let type = "post";
        let data = {
            id
        }

        $("html,body").css("cursor","progress;");

        ajPost(url,type,data,function(r){}, function(r){
            $("html,body").css("cursor","default;");
        }, false);

        // location.reload();
    })
})

function emailTableReload() {
    $inboxEmailTable.ajax.reload();
    $sendEmailTable.ajax.reload();
    $trashEmailTable.ajax.reload();
}

function colorPicker() {
    $(".color").spectrum({
        color: "#ECC",
        showInput: true,
        className: "full-spectrum",
        showInitial: true,
        showPalette: true,
        showSelectionPalette: true,
        maxSelectionSize: 10,
        preferredFormat: "hex",
        localStorageKey: "spectrum.demo",
        move: function (color) {

        },
        show: function () {

        },
        beforeShow: function () {

        },
        hide: function () {

        },
        change: function () {

        },
        palette: [
            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"
            ],
            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"
            ],
            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"
            ]
        ],
    });
}

function progressLoading(start = true) {

    var i = 0;

    var interval = setInterval(function () {
        var increment = i++;
        $(".progress-bar").width(increment + "%");
        $(".progress-bar").html(increment + "%");
        $(".progress-bar").css("background", "#fab95a");

        if (increment == 100) clearInterval(interval);

    }, 1000);

    if (!start) {
        clearInterval(interval);
    }


    return false;

    for (var x = 0, ln = 90; x < ln; x++) {
        setTimeout(function (y) {

        }, x * 300, x); // we're passing x
    }
}

function tableSearch(table, value) {
    console.log(value);
    $(table).filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

function selectAccount() {
    var defaultEmail = $("#email-account-select").val();
    $inboxEmailTable.columns(3).search(defaultEmail).draw();
    $sendEmailTable.columns(3).search(defaultEmail).draw();
    $trashEmailTable.columns(3).search(defaultEmail).draw();
}

function ajPost(url, type, data, callBack, statusCallBack, is_form = true) {
    if (is_form) {
        $.ajax({
            url,
            type,
            data,
            startTime: performance.now(),
            dataType: 'json',
            cache: false,
            processData: false,
            beforeSend: function () {

            },
            success: function (r) {
                callBack(r);
            },
            complete: function (xhr, textStatus) {
                statusCallBack(xhr.status)
            }
        })
    } else {
        $.ajax({
            url,
            type,
            data,
            dataType: 'json',
            success: function (r) {
                callBack(r);
            },
            complete: function (xhr, textStatus) {
                statusCallBack(xhr.status)
            }
        })
    }
}

function hideShow(attr, evt) {
    // hide
    if (evt == 'hide') {
        $(attr).addClass('d-none').removeClass('d-block');
    } else {
        // show
        $(attr).removeClass('d-none').addClass('d-block');
    }
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
