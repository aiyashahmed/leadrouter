$(function() {

});

function changeStepName(x) {
    var xid = x.id;
    var xvalue = x.value;

    var url = $base_url + '/pipeline_step/name_change';
    var data = {
        id: xid,
        value: xvalue
    }

    $.ajax({
        type: 'post',
        url: url,
        data: data,
        success: function(x) {
            pipelineReload();
        }
    });
}

function pipelineReload() {
    $dtHorizontalExample.ajax.reload();
}